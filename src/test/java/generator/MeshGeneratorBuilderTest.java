package generator;

import model.mesh.generator.CullingMeshGenerator;
import model.mesh.generator.GreedyMeshGenerator;
import model.mesh.generator.MeshGenerator;
import model.mesh.generator.MeshGeneratorBuilder;
import org.junit.Assert;
import org.junit.Test;

public class MeshGeneratorBuilderTest {

  @Test
  public void testBuilder() {
    MeshGeneratorBuilder builder = MeshGeneratorBuilder.of(GreedyMeshGenerator.class);
    MeshGenerator greedyGenerator = builder.build();
    Assert.assertNotNull(greedyGenerator);

    builder = MeshGeneratorBuilder.of(CullingMeshGenerator.class);
    MeshGenerator cullingGenerator = builder.build();
    Assert.assertNotNull(cullingGenerator);
  }
}
