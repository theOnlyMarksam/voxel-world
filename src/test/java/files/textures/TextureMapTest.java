package files.textures;

import model.block.Block;
import model.block.BlockType;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TextureMapTest {

    @Test
    public void testInit() throws Exception{
        TextureMap map = new TextureMap();
        map.initTextureMap();
        Map<BlockType, Block> blocks = map.getBlockInfo().getBlockMap();

        assertEquals(2, blocks.size());
    }
}
