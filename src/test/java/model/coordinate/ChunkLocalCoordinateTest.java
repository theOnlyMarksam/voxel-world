package model.coordinate;

import model.chunk.Chunk;
import org.junit.Assert;
import org.junit.Test;

public class ChunkLocalCoordinateTest {
  @Test
  public void testOfIndex() {
    int index = 0;

    for (int y = 0; y < Chunk.HEIGHT; y++) {
      for (int z = 0; z < Chunk.SIDE_LENGTH; z++) {
        for (int x = 0; x < Chunk.SIDE_LENGTH; x++) {
          ChunkLocalCoordinate c = ChunkLocalCoordinate.of(index);

          Assert.assertEquals(ChunkLocalCoordinate.of(x, y, z), c);

          index++;
        }
      }
    }
  }

  @Test
  public void testStuff() {
    ChunkLocalCoordinate crd = ChunkLocalCoordinate.of(256);

    Assert.assertEquals(ChunkLocalCoordinate.of(0, 1, 0), crd);
  }
}
