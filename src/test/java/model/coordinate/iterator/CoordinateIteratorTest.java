package model.coordinate.iterator;

import model.chunk.Chunk;
import model.coordinate.ChunkCoordinate;
import model.coordinate.ChunkLocalCoordinate;
import model.coordinate.GlobalCoordinate;
import org.junit.Assert;
import org.junit.Test;

public class CoordinateIteratorTest {
  @Test
  public void testIterator() {
    ChunkCoordinate c = ChunkCoordinate.of(0 , 0);
    int index = 0;

    for (GlobalCoordinate globalCoordinate : new CoordinateIterator(c)) {
      ChunkLocalCoordinate localCoordinate = ChunkLocalCoordinate.of(index);

      Assert.assertEquals(localCoordinate.x(), globalCoordinate.x());
      Assert.assertEquals(localCoordinate.y(), globalCoordinate.y());
      Assert.assertEquals(localCoordinate.z(), globalCoordinate.z());

      index++;
    }

    Assert.assertEquals(Chunk.CHUNK_BLOCK_COUNT, index);
  }
}
