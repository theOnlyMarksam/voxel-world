package model.coordinate.iterator;

import model.coordinate.ChunkCoordinate;
import org.joml.Vector3f;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.Iterator;
import java.util.stream.StreamSupport;

public class LandAreaChunkIterableTest {

  @Rule
  public Timeout globalTimeout = Timeout.millis(500);

  @Test
  public void testIterable() {
    LandAreaChunkIterable iterable = new LandAreaChunkIterable(new Vector3f(0, 0, 0), 1);
    Iterator<ChunkCoordinate> i = iterable.iterator();

    ChunkCoordinate next = i.next();
    Assert.assertEquals(ChunkCoordinate.of(-1, -1), next);
    Assert.assertTrue(i.hasNext());

    next = i.next();
    Assert.assertEquals(ChunkCoordinate.of(0, -1), next);
    Assert.assertTrue(i.hasNext());

    next = i.next();
    Assert.assertEquals(ChunkCoordinate.of(1, -1), next);
    Assert.assertTrue(i.hasNext());

    next = i.next();
    Assert.assertEquals(ChunkCoordinate.of(-1, 0), next);
    Assert.assertTrue(i.hasNext());

    next = i.next();
    Assert.assertEquals(ChunkCoordinate.of(0, 0), next);
    Assert.assertTrue(i.hasNext());

    next = i.next();
    Assert.assertEquals(ChunkCoordinate.of(1, 0), next);
    Assert.assertTrue(i.hasNext());

    next = i.next();
    Assert.assertEquals(ChunkCoordinate.of(-1, 1), next);
    Assert.assertTrue(i.hasNext());

    next = i.next();
    Assert.assertEquals(ChunkCoordinate.of(0, 1), next);
    Assert.assertTrue(i.hasNext());

    next = i.next();
    Assert.assertEquals(ChunkCoordinate.of(1, 1), next);
    Assert.assertFalse(i.hasNext());
  }

  @Test
  public void testStreaming() {
    StreamSupport.stream(new LandAreaChunkIterable(new Vector3f(0, 0, 0), 2).spliterator(), false)
        .forEach(System.out::println);
  }
}
