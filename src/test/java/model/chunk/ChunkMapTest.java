package model.chunk;

import org.joml.Vector2i;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChunkMapTest {

    /*@Test
    public void testInit() {
        Vector2i center = new Vector2i(10, 1);
        ChunkMap chunkMap = new ChunkMap(1, center);
        chunkMap.init();

        determineChunkCorrectness(center, 1, chunkMap);
    }

    @Test
    public void testGetChunk() {
        ChunkMap chunkMap = new ChunkMap(0, new Vector2i(-1, 1));
        for (int z = 0; z <= 2; z++) {
            for (int x = -2; x <= 0; x++) {
                chunkMap.set(new Chunk(x, z));
            }
        }

        determineChunkCorrectness(new Vector2i(-1, 1), 0, chunkMap);
    }

    @Test
    public void testOnPlayerChangeX() {
        ChunkMap chunkMap = new ChunkMap(0, new Vector2i(0, 0));

        for (int z = -1; z <= 1; z++) {
            for (int x = -1; x <= 1; x++) {
                chunkMap.set(new Chunk(x, z));
            }
        }

        chunkMap.onPlayerChunkChange(new Vector2i(1, 0));
        determineChunkCorrectness(new Vector2i(1, 0), 0, chunkMap);

        chunkMap.onPlayerChunkChange(new Vector2i(-2, 0));
        determineChunkCorrectness(new Vector2i(-1, 0), 0, chunkMap);
    }

    @Test
    public void testOnPlayerChangeZ() {
        ChunkMap chunkMap = new ChunkMap(0, new Vector2i(0, 0));

        for (int z = -1; z <= 1; z++) {
            for (int x = -1; x <= 1; x++) {
                chunkMap.set(new Chunk(x, z));
            }
        }

        chunkMap.onPlayerChunkChange(new Vector2i(0, 1));
        determineChunkCorrectness(new Vector2i(0, 1), 0, chunkMap);

        chunkMap.onPlayerChunkChange(new Vector2i(0, -2));
        determineChunkCorrectness(new Vector2i(0, -1), 0, chunkMap);
    }

    @Test
    public void testOnPlayerChangeBoth() {
        Vector2i center = new Vector2i(0, 0);
        ChunkMap chunkMap = new ChunkMap(0, new Vector2i(center.x(), center.y()));

        for (int z = -1; z <= 1; z++) {
            for (int x = -1; x <= 1; x++) {
                chunkMap.set(new Chunk(x, z));
            }
        }

        Vector2i change = new Vector2i(5, 5);
        center.add(change);

        chunkMap.onPlayerChunkChange(change);
        assertEquals(center, chunkMap.getCenter());

        determineChunkCorrectness(center, 0, chunkMap);

        change.set(-2, -2);
        center.add(change);

        chunkMap.onPlayerChunkChange(change);
        assertEquals(center, chunkMap.getCenter());

        determineChunkCorrectness(center, 0, chunkMap);
    }

    private void determineChunkCorrectness(Vector2i center, int radius, ChunkMap map) {
        for (int z = center.y() - radius - 1; z <= center.y() + radius + 1; z++) {
            for (int x = center.x() - radius - 1; x <= center.x() + radius + 1; x++) {
                assertEquals(new Chunk(x, z), map.get(x, z));
            }
        }
    }*/
}
