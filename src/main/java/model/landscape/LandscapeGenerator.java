package model.landscape;

import model.chunk.Chunk;

public interface LandscapeGenerator {

  void init();

  void generate(Chunk chunk);
}
