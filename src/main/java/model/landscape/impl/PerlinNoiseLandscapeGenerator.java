package model.landscape.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import common.conversion.CoordinateConverter;
import model.biome.Biome;
import model.biome.BiomeInfo;
import model.biome.BiomeType;
import model.block.BlockType;
import model.chunk.Chunk;
import model.coordinate.ChunkLocalCoordinate;
import model.coordinate.GlobalCoordinate;
import model.landscape.LandscapeGenerator;
import model.landscape.water.WaterBodyGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.*;
import procedural.noise.Perlin;

import java.io.IOException;
import java.lang.Math;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class PerlinNoiseLandscapeGenerator implements LandscapeGenerator {
  private static final Logger LOG = LogManager.getLogger(PerlinNoiseLandscapeGenerator.class);
  private static final int BIOME_SIZE = 128;
  private static final double TRANSITION = 32;

  private WaterBodyGenerator waterBodyGenerator = new WaterBodyGenerator();
  private final Perlin perlin = new Perlin();
  private BiomeInfo biomeInfo;
  private Map<Long, BiomeCenter> biomeCenterMap = new HashMap<>();

  private Random random = new Random(123953L);

  @Override
  public void init() {
    ObjectMapper om = new ObjectMapper();
    try {
      biomeInfo = om.readValue(
          getClass().getResourceAsStream("/properties/biomes/biomes.json"),
          BiomeInfo.class
      );
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void generate(Chunk chunk) {
    generateBiomes(chunk);
    generateHeightMap(chunk);
    waterBodyGenerator.generate(chunk);
  }

  private void generateBiomes(Chunk chunk) {
    for (int i = 0; i < 256; i++) {
      ChunkLocalCoordinate lPos = ChunkLocalCoordinate.of(i);
      GlobalCoordinate gPos = CoordinateConverter.chunkLocalToGlobal(i, chunk.getPosition());

      chunk.setBiomeType(lPos, getClosestBiomeCenter(gPos.x(), gPos.z()).type);
    }
  }

  private BiomeCenter getClosestBiomeCenter(double x, double z) {
    Vector2i biomePos = new Vector2i();
    convertGlobalCoordToBiomeCoord((int) x, (int) z, biomePos);

    BiomeCenter closest = calculateIfBiomeCenterAbsent(biomePos.x, biomePos.y);
    double distance = euclideanDistance(x, closest.x(), z, closest.y());

    for (int i = -2; i < 3; i++) {
      for (int j = -2; j < 3; j++) {
        if (i == 0 && j == 0) continue;

        BiomeCenter c = calculateIfBiomeCenterAbsent(biomePos.x + i, biomePos.y + j);
        double d = getModifiedDistance(x, z, c);

        if (d < distance) {
          distance = d;
          closest = c;
        }
      }
    }

    return closest;
  }

  private double getModifiedDistance(double x, double z, BiomeCenter c) {
    double f = 16.0;
    double amp = 32.0;
    double offsetX = perlin.noise(x / f, 0, z / f) * amp - amp / 2;
    double offsetZ = perlin.noise(z / f, 0, x / f) * amp - amp / 2;
    return euclideanDistance(x + offsetX, c.x(), z + offsetZ, c.y());
  }

  private double euclideanDistance(double x1, double x2, double y1, double y2) {
    return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
  }

  private BiomeCenter calculateIfBiomeCenterAbsent(int biomeX, int biomeZ) {
    BiomeCenter center = biomeCenterMap.get(coordinateToIndex(biomeX, biomeZ));

    if (center == null) {
      BiomeCenter createdCenter = calculateNewCenter(biomeX, biomeZ);
      biomeCenterMap.put(coordinateToIndex(biomeX, biomeZ), createdCenter);
      return createdCenter;
    }
    return center;
  }

  private BiomeCenter calculateNewCenter(int biomeX, int biomeZ) {
    BiomeCenter biomeCenter = new BiomeCenter();
    biomeCenter.location = convertBiomeCoordToGlobalCoord(biomeX, biomeZ).add(calculateBiomeCenterOffset(biomeX, biomeZ));
    biomeCenter.type = calculateBiomeType(biomeX, biomeZ);

    return biomeCenter;
  }

  private Vector2d calculateBiomeCenterOffset(int biomeX, int biomeZ) {
    Vector2d offset = new Vector2d();

    offset.x = random.nextDouble() * BIOME_SIZE - (BIOME_SIZE >> 1);
    offset.y = random.nextDouble() * BIOME_SIZE - (BIOME_SIZE >> 1);

    return offset;
  }

  private BiomeType calculateBiomeType(int biomeX, int biomeZ) {
    switch (Math.abs(((biomeX * biomeX) % 7) + (biomeZ - biomeX)) % 4) {
      case 0:
        return BiomeType.DESERT;
      case 1:
        return BiomeType.FOREST;
      case 2:
        return BiomeType.MOUNTAINS;
      case 3:
        return BiomeType.PLAINS;
      default:
        return BiomeType.PLAINS;
    }
  }

  private long coordinateToIndex(int biomeX, int biomeZ) {
    return (long) biomeX + (long) Integer.MAX_VALUE * (long) biomeZ;
  }

  private void convertGlobalCoordToBiomeCoord(int globalX, int globalZ, Vector2i biomePos) {
    int x = Math.floorDiv(globalX, BIOME_SIZE);
    int z = Math.floorDiv(globalZ, BIOME_SIZE);

    biomePos.set(x, z);
  }

  private Vector2d convertBiomeCoordToGlobalCoord(int biomeX, int biomeZ) {
    Vector2d gPos = new Vector2d();
    gPos.x = biomeX * BIOME_SIZE + (BIOME_SIZE >> 1);
    gPos.y = biomeZ * BIOME_SIZE + (BIOME_SIZE >> 1);

    return gPos;
  }

  private class BiomeCenter {
    Vector2d location;
    BiomeType type;

    double x() {
      return location.x;
    }

    double y() {
      return location.y;
    }
  }

  private void generateHeightMap(Chunk chunk) {
    for (int i = 0; i < 256; i++) {
      ChunkLocalCoordinate lPos = ChunkLocalCoordinate.of(i);
      GlobalCoordinate gPos = CoordinateConverter.chunkLocalToGlobal(i, chunk.getPosition());

      BiomeCenter closest = getClosestBiomeCenter(gPos.x(), gPos.z());
      double dstToClosest = getModifiedDistance(gPos.x(), gPos.z(), closest);

      List<BiomeCenter> transitions = biomeCenterMap
                                          .values()
                                          .stream()
                                          .filter(center ->
                                                      closest != center
                                                          && getModifiedDistance(gPos.x(), gPos.z(), center) - dstToClosest <= TRANSITION)
                                          .collect(Collectors.toList());

      Biome biome = biomeInfo.getBiome(chunk.getBiomeType(lPos));

      double height = calculateHeight(gPos.x(), gPos.z(), biome);
      double totalWeight = 1;

      for (BiomeCenter transition : transitions) {
        double w = 1 - (getModifiedDistance(gPos.x(), gPos.z(), transition) - dstToClosest) / TRANSITION;
        height += w * calculateHeight(gPos.x(), gPos.z(), biomeInfo.getBiome(transition.type));
        totalWeight += w;
      }

      height /= totalWeight;

      int waterHeight = 25;
      for (int h = 0; h <= height; h++) {
        chunk.setBlock(ChunkLocalCoordinate.of(lPos.x(), h, lPos.z()), biome.getBlockType());
      }

      if (height < waterHeight) {
        for (int h = (int) height + 1; h <= waterHeight; h++) {
          chunk.setBlock(ChunkLocalCoordinate.of(lPos.x(), h, lPos.z()), BlockType.WATER);
        }
      }
    }
  }

  private double calculateHeight(double x, double z, Biome biome) {
    double f = biome.getNoiseBaseFrequency();
    double amp = biome.getNoiseBaseAmplitude();
    double height = 0;

    for (int i = 0; i < biome.getNoiseLayerCount(); i++) {
      height += perlin.noise(x / f, 0, z / f) * amp;
      f /= 2;
      amp /= 2;
    }

    return height;
  }
}
