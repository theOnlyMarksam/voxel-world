package model.landscape.impl;

import model.block.BlockType;
import model.chunk.Chunk;
import model.coordinate.ChunkLocalCoordinate;
import model.landscape.LandscapeGenerator;

public class SimpleLandscapeGenerator implements LandscapeGenerator {

  @Override
  public void init() {

  }

  @Override
  public void generate(Chunk chunk) {
    for (int y = 0; y < 20; y++) {
      for (int z = 0; z < 16; z++) {
        for (int x = 0; x < 16; x++) {
          if ((chunk.x() + chunk.z()) % 2 == 0) {
            chunk.setBlock(ChunkLocalCoordinate.of(x, y, z), BlockType.GRASS);
          } else {
            chunk.setBlock(ChunkLocalCoordinate.of(x, y, z), BlockType.DIRT);
          }
        }
      }
    }
  }
}
