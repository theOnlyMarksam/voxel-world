package model;

import common.conversion.CoordinateConverter;
import entity.Player;
import files.textures.TextureMap;
import model.block.BlockType;
import model.chunk.Chunk;
import model.chunk.ChunkMap;
import model.coordinate.ChunkCoordinate;
import model.coordinate.ChunkLocalCoordinate;
import model.coordinate.GlobalCoordinate;
import model.coordinate.MeshCoordinate;
import model.coordinate.iterator.LandAreaChunkIterable;
import model.mesh.Mesh;
import model.mesh.MeshMap;
import model.mesh.Mesher;
import model.mesh.constants.MeshSeq;
import model.mesh.generator.CullingMeshGenerator;
import model.mesh.generator.MeshGeneratorBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector2i;
import org.joml.Vector3f;
import org.joml.Vector3i;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.StreamSupport;

public class ObservableWorld {
  private static final Logger LOG = LogManager.getLogger(ObservableWorld.class);
  private static final int RADIUS = 7;
  private static final float BLOCK_MODIFY_DISTANCE = 5.0f;
  private static final long TIME_LIMIT = 300000000L;
  private static final String LAND_MESH_GENERATOR = "landMeshGenerator";
  private static final String WATER_MESH_GENERATOR = "waterMeshGenerator";

  private TextureMap textureMap;
  private ChunkMap chunkMap;
  private MeshMap correctMeshMap;
  private Mesher mesher;
  private Player player;

  private Map<MeshCoordinate, Future<Mesh>> workList = new HashMap<>();
  private List<Mesh> changedMeshes = new ArrayList<>();
  private Collection<MeshCoordinate> deletedCoordinates = new ArrayList<>();

  private Vector3f lastPlayerPosition;

  private boolean addBlocks = false;
  private boolean destroyBlocks = false;
  private long lastBlockAdded = 0L;
  private long lastBlockRemoved = 0L;
  private BlockType blockToAdd;

  public void setBlockToAdd(BlockType blockToAdd) {
    this.blockToAdd = blockToAdd;
  }

  public ObservableWorld(Player player, TextureMap textureMap) {
    this.textureMap = textureMap;
    this.chunkMap = new ChunkMap();
    this.correctMeshMap = new MeshMap();
    this.mesher = new Mesher(chunkMap, textureMap);
    this.player = player;
  }

  public void init() {
    mesher.init();
    lastPlayerPosition = new Vector3f(player.getPosition());

    MeshGeneratorBuilder landMeshGenerator = MeshGeneratorBuilder.of(CullingMeshGenerator.class)
                                          .exclude(Arrays.asList(BlockType.AIR, BlockType.WATER))
                                          .strict();

    MeshGeneratorBuilder waterMeshGenerator = MeshGeneratorBuilder.of(CullingMeshGenerator.class)
                                           .include(Collections.singletonList(BlockType.WATER));

    mesher.addMeshGenerator(LAND_MESH_GENERATOR, landMeshGenerator);
    mesher.addMeshGenerator(WATER_MESH_GENERATOR, waterMeshGenerator);

    startMeshGeneration(player.getPosition());
  }

  private void startMeshGeneration(Vector3f center) {
    StreamSupport.stream(new LandAreaChunkIterable(center, RADIUS).spliterator(), false)
        .forEach(crd -> {
          if (!correctMeshMap.contains(crd)) {
            MeshCoordinate c1 = MeshCoordinate.of(crd, MeshSeq.LAND);
            MeshCoordinate c2 = MeshCoordinate.of(crd, MeshSeq.WATER);

            if (!workList.containsKey(c1)) {
              Future<Mesh> landMeshFuture = mesher.generateMesh(crd, MeshSeq.LAND, LAND_MESH_GENERATOR);
              workList.put(c1, landMeshFuture);
              LOG.debug("Started mesh generation for " + c1);
            }

            if (!workList.containsKey(c2)) {
              Future<Mesh> waterMeshFuture = mesher.generateMesh(crd, MeshSeq.WATER, WATER_MESH_GENERATOR);
              workList.put(c2, waterMeshFuture);
              LOG.debug("Started mesh generation for " + c2);
            }
          }
        });
  }

  public void update() {
    ChunkCoordinate lastChunkPos = CoordinateConverter.globalToChunk(lastPlayerPosition);
    ChunkCoordinate currentChunkPos = CoordinateConverter.globalToChunk(player.getPosition());

    if (lastChunkPos.x() != currentChunkPos.x() || lastChunkPos.z() != currentChunkPos.z()) {
      startMeshGeneration(player.getPosition());
    }

    deletedCoordinates = correctMeshMap.reduceSizeTo(getMaxAllowedMeshCount(), player.getPosition());

    if (addBlocks && (System.nanoTime() - lastBlockAdded) > TIME_LIMIT) {
      placeBlockIfInRadius();
    }

    if (destroyBlocks && !addBlocks && (System.nanoTime() - lastBlockRemoved) > TIME_LIMIT) {
      removeBlockIfInRadius();
    }

    lastPlayerPosition.set(player.getPosition());
  }

  private int getMaxAllowedMeshCount() {
    return (RADIUS * 2 + 1) * (RADIUS * 2 + 1) * MeshSeq.values().length;
  }

  public Collection<Mesh> getNewOrChangedMeshes() {
    List<MeshCoordinate> cancelled = new ArrayList<>();
    Map<MeshCoordinate, Mesh> completed = new HashMap<>();

    workList.entrySet()
        .stream()
        .filter(entry -> entry.getValue().isDone())
        .forEach(entry -> {
          try {
            completed.put(entry.getKey(), entry.getValue().get());
          } catch (CancellationException e) {
            cancelled.add(entry.getKey());
          } catch (ExecutionException e) {
            throw new RuntimeException(e);
          } catch (InterruptedException e) {
            LOG.warn("Current thread was interrupted while waiting for mesh: " + entry.getKey());
          }
        });

    cancelled.forEach(crd -> workList.remove(crd));
    completed.forEach((crd, mesh) -> {
      mesh.setHasChanged(false);
      workList.remove(crd);
      correctMeshMap.add(mesh);
    });

    changedMeshes.forEach(mesh -> {
      mesh.setHasChanged(false);
      correctMeshMap.add(mesh);
    });

    List<Mesh> result = new ArrayList<>(completed.values());
    result.addAll(changedMeshes);
    changedMeshes.clear();

    return result;
  }

  public Collection<MeshCoordinate> getDeletedMeshCoordinates() {
    return deletedCoordinates;
  }

  private void removeBlockIfInRadius() {
    float currentDistance = 0f;
    Vector3f currentPos = new Vector3f(player.getPosition());

    // if player is inside a block
    if (chunkMap.getBlock(currentPos) != BlockType.AIR) return;

    float resolution = 0.1f;
    Vector3f step = calculateStep(resolution);

    while (currentDistance < BLOCK_MODIFY_DISTANCE) {
      if (chunkMap.getBlock(currentPos) != BlockType.AIR) {
        chunkMap.setBlock(currentPos, BlockType.AIR);
        lastBlockRemoved = System.nanoTime();
        updateMeshes(currentPos);
        break;
      }

      currentPos.add(step);
      currentDistance += resolution;
    }
  }

  private void placeBlockIfInRadius() {
    float currentDistance = 0f;
    Vector3f lastPos = new Vector3f(player.getPosition());
    Vector3f currentPos = new Vector3f(player.getPosition());

    // if player is inside a block
    if (chunkMap.getBlock(currentPos) != BlockType.AIR) return;

    float resolution = 0.1f;
    Vector3f step = calculateStep(resolution);

    while (currentDistance < BLOCK_MODIFY_DISTANCE) {
      if (chunkMap.getBlock(currentPos) != BlockType.AIR && !isSameBlock(lastPos, player.getPosition())) {
        chunkMap.setBlock(lastPos, blockToAdd);
        lastBlockAdded = System.nanoTime();
        updateMeshes(currentPos);
        break;
      }

      lastPos.set(currentPos);

      currentPos.add(step);
      currentDistance += resolution;
    }
  }

  private Vector3f calculateStep(float resolution) {
    Vector3f direction = player.getLookAt();
    float lenght = direction.length();

    Vector3f result = new Vector3f();
    direction.div(lenght, result);

    return result.mul(resolution);
  }

  private boolean isSameBlock(Vector3f vec1, Vector3f vec2) {
    Vector2i chunk1 = getChunkCoords(vec1);
    Vector2i chunk2 = getChunkCoords(vec2);

    if (!chunk1.equals(chunk2)) return false;

    return getBlockLocalCoords(vec1, chunk1).equals(getBlockLocalCoords(vec2, chunk2));
  }

  private Vector2i getChunkCoords(Vector3f globalCoords) {
    Vector2i res = new Vector2i();

    res.x = Math.floorDiv((int) Math.floor(globalCoords.x), 16);
    res.y = Math.floorDiv((int) Math.floor(globalCoords.z), 16);

    return res;
  }

  private Vector3i getBlockLocalCoords(Vector3f globalCoords, Vector2i chunkCoords) {
    Vector3i res = new Vector3i();

    res.x = (int) Math.floor(globalCoords.x - chunkCoords.x * 16);
    res.y = (int) globalCoords.y;
    res.z = (int) Math.floor(globalCoords.z - chunkCoords.y * 16);

    return res;
  }

  private void updateMeshes(Vector3f currentPos) {
    GlobalCoordinate gPos = GlobalCoordinate.of((int) currentPos.x, (int) currentPos.y, (int) currentPos.z);
    ChunkLocalCoordinate chunkLocalCoordinate = CoordinateConverter.globalToChunkGlobal(gPos).getChunkLocalCoordinate();
    ChunkCoordinate chunkCrd = CoordinateConverter.globalToChunk(currentPos);

    changedMeshes.add(mesher.generateMeshNow(chunkCrd, MeshSeq.LAND, LAND_MESH_GENERATOR));
    changedMeshes.add(mesher.generateMeshNow(chunkCrd, MeshSeq.WATER, WATER_MESH_GENERATOR));

    if (chunkLocalCoordinate.x() == 0) {
      ChunkCoordinate crd = ChunkCoordinate.of(chunkCrd.x() - 1, chunkCrd.z());

      changedMeshes.add(mesher.generateMeshNow(crd, MeshSeq.LAND, LAND_MESH_GENERATOR));
      changedMeshes.add(mesher.generateMeshNow(crd, MeshSeq.WATER, WATER_MESH_GENERATOR));
    } else if (chunkLocalCoordinate.x() == Chunk.SIDE_LENGTH - 1) {
      ChunkCoordinate crd = ChunkCoordinate.of(chunkCrd.x() + 1, chunkCrd.z());

      changedMeshes.add(mesher.generateMeshNow(crd, MeshSeq.LAND, LAND_MESH_GENERATOR));
      changedMeshes.add(mesher.generateMeshNow(crd, MeshSeq.WATER, WATER_MESH_GENERATOR));
    }

    if (chunkLocalCoordinate.z() == 0) {
      ChunkCoordinate crd = ChunkCoordinate.of(chunkCrd.x(), chunkCrd.z() - 1);

      changedMeshes.add(mesher.generateMeshNow(crd, MeshSeq.LAND, LAND_MESH_GENERATOR));
      changedMeshes.add(mesher.generateMeshNow(crd, MeshSeq.WATER, WATER_MESH_GENERATOR));
    } else if (chunkLocalCoordinate.z() == Chunk.SIDE_LENGTH - 1) {
      ChunkCoordinate crd = ChunkCoordinate.of(chunkCrd.x(), chunkCrd.z() + 1);

      changedMeshes.add(mesher.generateMeshNow(crd, MeshSeq.LAND, LAND_MESH_GENERATOR));
      changedMeshes.add(mesher.generateMeshNow(crd, MeshSeq.WATER, WATER_MESH_GENERATOR));
    }
  }

  public void shutdown() {
    mesher.shutdown();
    workList.clear();
    deletedCoordinates.clear();
    changedMeshes.clear();
    correctMeshMap.shutdown();
  }

  public void setDestroyBlocks(boolean b) {
    this.destroyBlocks = b;
    lastBlockRemoved = 0L;
  }

  public void setAddBlocks(boolean b) {
    this.addBlocks = b;
    lastBlockAdded = 0L;
  }
}
