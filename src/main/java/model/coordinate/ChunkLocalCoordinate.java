package model.coordinate;

import model.chunk.Chunk;

public class ChunkLocalCoordinate {
  private int x;
  private int layer;
  private int z;

  public static ChunkLocalCoordinate of(int x, int y, int z) {
    if (x < 0 || x >= Chunk.SIDE_LENGTH)
      throw new IllegalArgumentException(String.format("chunk local x must be in range [0, %d], but was %d", Chunk.SIDE_LENGTH - 1, x));

    if (y < 0 || y >= Chunk.HEIGHT)
      throw new IllegalArgumentException(String.format("chunk local y must be in range [0, %d], but was %d", Chunk.HEIGHT - 1, y));

    if (z < 0 || z >= Chunk.SIDE_LENGTH)
      throw new IllegalArgumentException(String.format("chunk local z must be in range [0, %d], but was %d", Chunk.SIDE_LENGTH - 1, z));

    return new ChunkLocalCoordinate(x, y, z);
  }

  public static ChunkLocalCoordinate of(int index) {
    int maxIndex = Chunk.LAYER_SIZE * Chunk.HEIGHT - 1;

    if (index < 0 || index > maxIndex)
      throw new IllegalArgumentException(
          String.format(
              "block index must be in range [0, %d], but was %d"
              , maxIndex
              , index
          )
      );

    int y = Math.floorDiv(index, Chunk.LAYER_SIZE);
    int z = Math.floorDiv(index - y * Chunk.LAYER_SIZE, Chunk.SIDE_LENGTH);
    int x = index - y * Chunk.LAYER_SIZE - z * Chunk.SIDE_LENGTH;

    return new ChunkLocalCoordinate(x, y, z);
  }

  private ChunkLocalCoordinate(int x, int y, int z) {
    this.x = x;
    this.layer = y;
    this.z = z;
  }

  public int x() {
    return x;
  }

  public int y() {
    return layer;
  }

  public int z() {
    return z;
  }

  public int getIndex() {
    return layer * Chunk.LAYER_SIZE + z * Chunk.SIDE_LENGTH + x;
  }

  public int getColumnIndex() {
    return x + z * Chunk.SIDE_LENGTH;
  }

  @Override
  public int hashCode() {
    int result = x;
    result = 31 * result + layer;
    result = 31 * result + z;
    return result;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ChunkLocalCoordinate that = (ChunkLocalCoordinate) o;
    return x == that.x && layer == that.layer && z == that.z;
  }

  @Override
  public String toString() {
    return "ChunkLocalCoordinate{" +
               "x=" + x +
               ", layer=" + layer +
               ", z=" + z +
               '}';
  }
}
