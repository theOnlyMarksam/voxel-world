package model.coordinate;

import model.mesh.constants.MeshSeq;

public class MeshCoordinate {
  private ChunkCoordinate chunkCoordinate;
  private MeshSeq sequenceNr;

  public static MeshCoordinate of(ChunkCoordinate chunkCoordinate, MeshSeq sequenceNr) {
    return new MeshCoordinate(chunkCoordinate, sequenceNr);
  }

  private MeshCoordinate(ChunkCoordinate chunkCoordinate, MeshSeq sequenceNr) {
    this.chunkCoordinate = chunkCoordinate;
    this.sequenceNr = sequenceNr;
  }

  public ChunkCoordinate getChunkCoordinate() {
    return chunkCoordinate;
  }

  public MeshSeq getSequenceNr() {
    return sequenceNr;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    MeshCoordinate that = (MeshCoordinate) o;

    if (sequenceNr != that.sequenceNr) return false;
    return chunkCoordinate.equals(that.chunkCoordinate);
  }

  @Override
  public int hashCode() {
    int result = chunkCoordinate.hashCode();
    result = 31 * result + sequenceNr.getSeqNr();
    return result;
  }

  @Override
  public String toString() {
    return "MeshCoordinate{" +
               "chunkCoordinate=" + chunkCoordinate +
               ", sequenceNr=" + sequenceNr +
               '}';
  }
}
