package model.coordinate.iterator;

import model.chunk.Chunk;
import model.coordinate.ChunkCoordinate;
import model.coordinate.GlobalCoordinate;

import java.util.Iterator;

public class CoordinateIterator implements Iterable<GlobalCoordinate> {

  private ChunkCoordinate coordinate;

  public CoordinateIterator(ChunkCoordinate coordinate) {
    this.coordinate = coordinate;
  }

  @Override
  public Iterator<GlobalCoordinate> iterator() {
    return new Iterator<GlobalCoordinate>() {
      int index = 0;
      int x = -1;
      int y = -1;
      int z = -1;

      @Override
      public boolean hasNext() {
        return index < Chunk.CHUNK_BLOCK_COUNT;
      }

      @Override
      public GlobalCoordinate next() {
        x = (x + 1) % Chunk.SIDE_LENGTH;

        if (x == 0)
          z = (z + 1) % Chunk.SIDE_LENGTH;

        if (x == 0 && z == 0)
          y++;

        index++;

        return GlobalCoordinate.of(
            x + coordinate.x() * Chunk.SIDE_LENGTH,
            y,
            z + coordinate.z() * Chunk.SIDE_LENGTH
        );
      }
    };
  }
}
