package model.coordinate.iterator;

import common.conversion.CoordinateConverter;
import model.chunk.Chunk;
import model.coordinate.ChunkCoordinate;
import org.joml.Vector3f;

import java.util.Iterator;

public class LandAreaChunkIterable implements Iterable<ChunkCoordinate> {

  private ChunkCoordinate center;
  private int radius;

  public LandAreaChunkIterable(Vector3f center, int radius) {
    this.center = CoordinateConverter.globalToChunk(center);
    this.radius = Math.abs(radius);
  }

  @Override
  public Iterator<ChunkCoordinate> iterator() {
    return new LandAreaIterator();
  }

  private class LandAreaIterator implements Iterator<ChunkCoordinate> {
    int shiftX = -radius;
    int shiftZ = -radius;
    ChunkCoordinate nextValid;

    LandAreaIterator() {
      this.nextValid = computeNextValid();
    }

    @Override
    public boolean hasNext() {
      return nextValid != null;
    }

    @Override
    public ChunkCoordinate next() {
      ChunkCoordinate currentValid = nextValid;
      nextValid = computeNextValid();
      return currentValid;
    }

    private ChunkCoordinate computeNextValid() {
      ChunkCoordinate crd = ChunkCoordinate.of(center.x() + shiftX, center.z() + shiftZ);

      while (crd.distanceInChunkUnits(center) > (double) radius + Math.sqrt(2) * 0.5) {
        shiftToNext();

        if (shiftZ > radius) {
          return null;
        }

        crd = ChunkCoordinate.of(center.x() + shiftX, center.z() + shiftZ);
      }

      shiftToNext();

      return crd;
    }

    private void shiftToNext() {
      shiftX++;
      if (shiftX > radius) {
        shiftX = -radius;
        shiftZ++;
      }
    }
  }
}
