package model.coordinate;

public class ChunkCoordinate {
  private int x;
  private int z;

  public static ChunkCoordinate of(int x, int z) {
    return new ChunkCoordinate(x, z);
  }

  public static ChunkCoordinate of(ChunkCoordinate coordinate) {
    return new ChunkCoordinate(coordinate.x, coordinate.z);
  }

  private ChunkCoordinate(int x, int z) {
    this.x = x;
    this.z = z;
  }

  public float distanceInChunkUnits(ChunkCoordinate coordinate) {
    return (float) Math.sqrt(Math.pow(x - coordinate.x, 2) + Math.pow(z - coordinate.z, 2));
  }

  public float distanceInChunkUnits(MeshCoordinate coordinate) {
    return distanceInChunkUnits(coordinate.getChunkCoordinate());
  }

  public int x() {
    return x;
  }

  public int z() {
    return z;
  }

  @Override
  public int hashCode() {
    return x + z * (0xFFFFFFFF >> 4);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ChunkCoordinate that = (ChunkCoordinate) o;
    return x == that.x && z == that.z;
  }

  @Override
  public String toString() {
    return "ChunkCoordinate{" +
               "x=" + x +
               ", z=" + z +
               '}';
  }
}
