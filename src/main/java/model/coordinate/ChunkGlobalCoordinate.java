package model.coordinate;

public class ChunkGlobalCoordinate {
    private ChunkCoordinate chunkCoordinate;
    private ChunkLocalCoordinate chunkLocalCoordinate;

    public static ChunkGlobalCoordinate of(ChunkCoordinate chunkCoordinate,
                                            ChunkLocalCoordinate chunkLocalCoordinate) {
        return new ChunkGlobalCoordinate(chunkCoordinate, chunkLocalCoordinate);
    }

    public static ChunkGlobalCoordinate of(int chunkX, int chunkZ, int localX, int localY, int localZ) {
        return new ChunkGlobalCoordinate(
                ChunkCoordinate.of(chunkX, chunkZ),
                ChunkLocalCoordinate.of(localX, localY, localZ)
        );
    }

    private ChunkGlobalCoordinate(ChunkCoordinate chunkCoordinate,
                                  ChunkLocalCoordinate chunkLocalCoordinate) {
        this.chunkCoordinate = chunkCoordinate;
        this.chunkLocalCoordinate = chunkLocalCoordinate;
    }

    public ChunkCoordinate getChunkCoordinate() {
        return chunkCoordinate;
    }

    public ChunkLocalCoordinate getChunkLocalCoordinate() {
        return chunkLocalCoordinate;
    }

    public int getChunkX() {
        return chunkCoordinate.x();
    }

    public int getChunkZ() {
        return chunkCoordinate.z();
    }

    public int getLocalX() {
        return chunkLocalCoordinate.x();
    }

    public int getLocalY() {
        return chunkLocalCoordinate.y();
    }

    public int getLocalZ() {
        return chunkLocalCoordinate.z();
    }

    @Override
    public int hashCode() {
        int result = chunkCoordinate.hashCode();
        result = 31 * result + chunkLocalCoordinate.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChunkGlobalCoordinate that = (ChunkGlobalCoordinate) o;

        if (!chunkCoordinate.equals(that.chunkCoordinate)) return false;
        return chunkLocalCoordinate.equals(that.chunkLocalCoordinate);
    }
}
