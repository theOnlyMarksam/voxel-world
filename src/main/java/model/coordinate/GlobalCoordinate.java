package model.coordinate;

public class GlobalCoordinate {
  private long x;
  private long y;
  private long z;

  public static GlobalCoordinate of(long x, long y, long z) {
    return new GlobalCoordinate(x, y, z);
  }

  private GlobalCoordinate(long x, long y, long z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public long x() {
    return x;
  }

  public long y() {
    return y;
  }

  public long z() {
    return z;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    GlobalCoordinate that = (GlobalCoordinate) o;
    return x == that.x && y == that.y && z == that.z;
  }

  @Override
  public int hashCode() {
    int result = (int) (x ^ (x >>> 32));
    result = 31 * result + (int) (y ^ (y >>> 32));
    result = 31 * result + (int) (z ^ (z >>> 32));
    return result;
  }
}
