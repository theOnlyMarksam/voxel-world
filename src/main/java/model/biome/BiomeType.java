package model.biome;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum BiomeType {
  @JsonProperty("desert")
  DESERT,
  @JsonProperty("forest")
  FOREST,
  @JsonProperty("mountains")
  MOUNTAINS,
  @JsonProperty("plains")
  PLAINS,
  @JsonProperty("swamp")
  SWAMP,
  @JsonProperty("ocean")
  OCEAN
}
