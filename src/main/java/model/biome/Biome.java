package model.biome;

import model.block.BlockType;

import java.util.List;

public class Biome {
  private BlockType blockType;
  private List<Palette> palettes;
  private NoiseParams noiseParams;

  private static class NoiseParams {
    String type;
    int layerCount;
    double baseFrequency;
    double baseAmplitude;

    public void setType(String type) {
      this.type = type;
    }

    public void setLayerCount(int layerCount) {
      this.layerCount = layerCount;
    }

    public void setBaseFrequency(double baseFrequency) {
      this.baseFrequency = baseFrequency;
    }

    public void setBaseAmplitude(double baseAmplitude) {
      this.baseAmplitude = baseAmplitude;
    }
  }

  public BlockType getBlockType() {
    return blockType;
  }

  public void setBlockType(BlockType blockType) {
    this.blockType = blockType;
  }

  public List<Palette> getPalettes() {
    return palettes;
  }

  public void setPalettes(List<Palette> palettes) {
    this.palettes = palettes;
  }

  public NoiseParams getNoiseParams() {
    return noiseParams;
  }

  public void setNoiseParams(NoiseParams noiseParams) {
    this.noiseParams = noiseParams;
  }

  public int getNoiseLayerCount() {
    return noiseParams.layerCount;
  }

  public double getNoiseBaseFrequency() {
    return noiseParams.baseFrequency;
  }

  public double getNoiseBaseAmplitude() {
    return noiseParams.baseAmplitude;
  }
}
