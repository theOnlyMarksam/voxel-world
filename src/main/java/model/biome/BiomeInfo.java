package model.biome;

import java.util.Map;

public class BiomeInfo {
  private Map<BiomeType, Biome> biomeInfo;

  public Biome getBiome(BiomeType biomeType) {
    return biomeInfo.get(biomeType);
  }

  public Map<BiomeType, Biome> getBiomeInfo() {
    return biomeInfo;
  }

  public void setBiomeInfo(Map<BiomeType, Biome> biomeInfo) {
    this.biomeInfo = biomeInfo;
  }
}
