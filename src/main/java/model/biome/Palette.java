package model.biome;

import model.block.BlockType;
import model.mesh.constants.CubeFace;

public class Palette {
  private BlockType blockType;
  private CubeFace side;
  private String name;
  private Coordinate coordinate;

  private static class Coordinate {
    double u;
    double v;

    public double getU() {
      return u;
    }

    public void setU(double u) {
      this.u = u;
    }

    public double getV() {
      return v;
    }

    public void setV(double v) {
      this.v = v;
    }
  }

  public BlockType getBlockType() {
    return blockType;
  }

  public void setBlockType(BlockType blockType) {
    this.blockType = blockType;
  }

  public CubeFace getSide() {
    return side;
  }

  public void setSide(CubeFace side) {
    this.side = side;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Coordinate getCoordinate() {
    return coordinate;
  }

  public void setCoordinate(Coordinate coordinate) {
    this.coordinate = coordinate;
  }
}
