package model.gui.text;

import files.font.bitmap.Font;
import main.ApplicationContext;
import mechanics.element.FontMeshGenerator;
import mechanics.element.GuiElementMesh;
import mechanics.element.Rectangle;

public class GuiText {
  private final String text;
  private GuiElementMesh mesh;
  private Rectangle container;
  private PositioningStrategyType positioning;
  private int width;
  private int height;

  public GuiText(String text, Rectangle container, PositioningStrategyType positioning) {
    this.text = text;
    this.mesh = FontMeshGenerator.generateMesh(text);
    this.container = container;
    this.positioning = positioning;
    this.width = calculateWidth(text);
    this.height = ApplicationContext.getFont().getFontCommon().getLineHeight();
  }

  private int calculateWidth(String text) {
    Font font = ApplicationContext.getFont();
    int result = 0;

    for (int i = 0; i < text.length(); i++) {
      result += font.getChar(text.charAt(i)).getxAdvance();
    }

    return result;
  }

  public String getText() {
    return text;
  }

  public GuiElementMesh getMesh() {
    return mesh;
  }

  public Rectangle getContainer() {
    return container;
  }

  public PositioningStrategyType getPositioning() {
    return positioning;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public enum PositioningStrategyType {
    XY_CENTER
  }
}
