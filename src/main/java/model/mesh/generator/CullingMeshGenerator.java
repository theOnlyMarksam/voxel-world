package model.mesh.generator;

import files.textures.TextureMap;
import model.block.BlockType;
import model.chunk.ChunkMap;
import model.coordinate.ChunkCoordinate;
import model.coordinate.GlobalCoordinate;
import model.coordinate.MeshCoordinate;
import model.coordinate.iterator.CoordinateIterator;
import model.mesh.constants.CubeFace;
import model.mesh.Mesh;
import model.mesh.constants.MeshSeq;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.function.Predicate;

public final class CullingMeshGenerator extends AbstractMeshGenerator {
  private static final Logger LOG = LogManager.getFormatterLogger(CullingMeshGenerator.class);

  private CullingMeshGenerator(Collection<BlockType> include,
                               Collection<BlockType> exclude,
                               ChunkMap chunkMap,
                               TextureMap textureMap,
                               boolean strict) {
    super(include, exclude, chunkMap, textureMap, strict);
  }

  @Override
  public Mesh generate(ChunkCoordinate coordinate, MeshSeq sequenceNumber) {
    long startTime = System.nanoTime();
    Mesh mesh = new Mesh(MeshCoordinate.of(coordinate, sequenceNumber));
    mesh.setHasChanged(true);

    if (include == null) {
      useExcludedOnly(mesh, coordinate);
    } else if (exclude.size() == 1 && exclude.contains(BlockType.AIR)) {
      useIncludeOnly(mesh, coordinate);
    } else {
      useExcludeAndInclude(mesh);
    }

    float totalTime = (System.nanoTime() - startTime) / 1e9f;
    LOG.debug("Mesh %s with %d vertices completed in %.4f s", mesh.getPosition(), mesh.getVertexCount(), totalTime);
    return mesh;
  }

  private void useExcludedOnly(Mesh mesh, ChunkCoordinate coordinate) {
    if (strict) {
      strictExcluded(mesh, coordinate);
    } else {
      nonStrictExcluded(mesh, coordinate);
    }
  }

  private void strictExcluded(Mesh mesh, ChunkCoordinate coordinate) {
    for (GlobalCoordinate gPos : new CoordinateIterator(coordinate)) {
      Blocks blocks = new Blocks(gPos).invoke();

      Predicate<BlockType> test = exclude::contains;

      if (!exclude.contains(blocks.currentBlock)) {
        addVerticesForAllSides(mesh, gPos, blocks, test);
      }
    }
  }

  private void nonStrictExcluded(Mesh mesh, ChunkCoordinate coordinate) {
    for (GlobalCoordinate gPos : new CoordinateIterator(coordinate)) {
      Blocks blocks = new Blocks(gPos).invoke();

      Predicate<BlockType> test = b -> exclude.contains(b) && b == BlockType.AIR;

      if (!exclude.contains(blocks.currentBlock)) {
        addVerticesForAllSides(mesh, gPos, blocks, test);
      }
    }
  }

  private void useIncludeOnly(Mesh mesh, ChunkCoordinate coordinate) {
    if (strict) {
      throw new UnsupportedOperationException();
    } else {
      nonStrictIncluded(mesh, coordinate);
    }
  }

  private void nonStrictIncluded(Mesh mesh, ChunkCoordinate coordinate) {
    for (GlobalCoordinate gPos : new CoordinateIterator(coordinate)) {
      Blocks blocks = new Blocks(gPos).invoke();

      Predicate<BlockType> test = b -> b == BlockType.AIR;

      if (include.contains(blocks.currentBlock)) {
        addVerticesForAllSides(mesh, gPos, blocks, test);
      }
    }
  }

  private void addVerticesForAllSides(Mesh mesh, GlobalCoordinate gPos, Blocks blocks, Predicate<BlockType> test) {
    addVerticesIfNeeded(mesh, gPos, blocks.currentBlock, blocks.left, CubeFace.LEFT, test);
    addVerticesIfNeeded(mesh, gPos, blocks.currentBlock, blocks.right, CubeFace.RIGHT, test);
    addVerticesIfNeeded(mesh, gPos, blocks.currentBlock, blocks.above, CubeFace.TOP, test);
    addVerticesIfNeeded(mesh, gPos, blocks.currentBlock, blocks.below, CubeFace.BOTTOM, test);
    addVerticesIfNeeded(mesh, gPos, blocks.currentBlock, blocks.front, CubeFace.FRONT, test);
    addVerticesIfNeeded(mesh, gPos, blocks.currentBlock, blocks.behind, CubeFace.BACK, test);
  }

  private void addVerticesIfNeeded(Mesh mesh,
                                   GlobalCoordinate gPos,
                                   BlockType currentBlock,
                                   BlockType testedBlock,
                                   CubeFace cubeFace,
                                   Predicate<BlockType> predicate) {
    if (predicate.test(testedBlock)) {
      mesh.getVertexList().add(cubeFace, gPos, textureMap.loadAndGetIndex(currentBlock, cubeFace));
    }
  }

  private void useExcludeAndInclude(Mesh mesh) {
    throw new UnsupportedOperationException();
  }

  private class Blocks {
    private GlobalCoordinate gPos;
    BlockType currentBlock;
    BlockType left;
    BlockType right;
    BlockType above;
    BlockType below;
    BlockType front;
    BlockType behind;

    Blocks(GlobalCoordinate gPos) {
      this.gPos = gPos;
    }

    Blocks invoke() {
      currentBlock = chunkMap.getBlock(gPos);

      left = chunkMap.left(gPos);
      right = chunkMap.right(gPos);
      above = chunkMap.above(gPos);
      below = chunkMap.below(gPos);
      front = chunkMap.front(gPos);
      behind = chunkMap.behind(gPos);
      return this;
    }
  }
}
