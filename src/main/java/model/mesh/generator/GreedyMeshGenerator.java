package model.mesh.generator;

import files.textures.TextureMap;
import model.block.BlockType;
import model.chunk.ChunkMap;
import model.coordinate.ChunkCoordinate;
import model.mesh.Mesh;
import model.mesh.constants.MeshSeq;

import java.util.Collection;

public final class GreedyMeshGenerator extends AbstractMeshGenerator {

  private GreedyMeshGenerator(Collection<BlockType> include,
                              Collection<BlockType> exclude,
                              ChunkMap chunkMap,
                              TextureMap textureMap,
                              boolean strict) {
    super(include, exclude, chunkMap, textureMap, strict);
  }

  @Override
  public Mesh generate(ChunkCoordinate coordinate, MeshSeq sequenceNumber) {
    throw new UnsupportedOperationException("Not implemented");
  }
}
