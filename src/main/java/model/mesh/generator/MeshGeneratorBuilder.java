package model.mesh.generator;

import files.textures.TextureMap;
import model.block.BlockType;
import model.chunk.ChunkMap;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;

public class MeshGeneratorBuilder {
  private final Class<? extends AbstractMeshGenerator> clazz;
  private Collection<BlockType> include;
  private Collection<BlockType> exclude = Collections.singletonList(BlockType.AIR);
  private ChunkMap chunkMap;
  private TextureMap textureMap;
  private boolean strict = false;

  private MeshGeneratorBuilder(Class<? extends AbstractMeshGenerator> clazz) {
    this.clazz = clazz;
  }

  public static MeshGeneratorBuilder of(Class<? extends AbstractMeshGenerator> clazz) {
    return new MeshGeneratorBuilder(clazz);
  }

  public MeshGeneratorBuilder include(Collection<BlockType> include) {
    this.include = include;
    return this;
  }

  public MeshGeneratorBuilder exclude(Collection<BlockType> exclude) {
    this.exclude = exclude;
    return this;
  }

  public MeshGeneratorBuilder chunkMap(ChunkMap chunkMap) {
    this.chunkMap = chunkMap;
    return this;
  }

  public MeshGeneratorBuilder textureMap(TextureMap textureMap) {
    this.textureMap = textureMap;
    return this;
  }

  public MeshGeneratorBuilder strict() {
    this.strict = true;
    return this;
  }

  public MeshGenerator build() {
    if (chunkMap == null) throw new NullPointerException("ChunkMap can not be null");
    if (textureMap == null) throw new NullPointerException("TextureMap can not be null");

    if (include == null && exclude == null) {
      throw new NullPointerException("Exclude and include collections can not both be null");
    }

    if (include != null && exclude != null && !Collections.disjoint(exclude, include)) {
      throw new IllegalArgumentException("Exclude and include collections must be disjoint");
    }

    try {
      Constructor<? extends AbstractMeshGenerator> constructor = getConstructor(clazz);
      constructor.setAccessible(true);

      return constructor.newInstance(
          include,
          exclude,
          chunkMap,
          textureMap,
          strict);
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      throw new RuntimeException(e.toString());
    }
  }

  private Constructor<? extends AbstractMeshGenerator> getConstructor(Class<? extends AbstractMeshGenerator> clazz)
      throws NoSuchMethodException {
    return clazz.getDeclaredConstructor(
        Collection.class,
        Collection.class,
        ChunkMap.class,
        TextureMap.class,
        boolean.class
    );
  }
}
