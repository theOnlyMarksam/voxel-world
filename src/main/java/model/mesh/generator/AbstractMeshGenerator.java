package model.mesh.generator;

import files.textures.TextureMap;
import model.block.BlockType;
import model.chunk.ChunkMap;

import java.util.Collection;

abstract class AbstractMeshGenerator implements MeshGenerator {
  final Collection<BlockType> include;
  final Collection<BlockType> exclude;
  final ChunkMap chunkMap;
  final TextureMap textureMap;
  final boolean strict;

  AbstractMeshGenerator(Collection<BlockType> include,
                        Collection<BlockType> exclude,
                        ChunkMap chunkMap,
                        TextureMap textureMap,
                        boolean strict) {
    this.include = include;
    this.exclude = exclude;
    this.chunkMap = chunkMap;
    this.textureMap = textureMap;
    this.strict = strict;
  }
}
