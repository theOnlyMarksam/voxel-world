package model.mesh.generator;

import model.coordinate.ChunkCoordinate;
import model.mesh.Mesh;
import model.mesh.constants.MeshSeq;

public interface MeshGenerator {
  Mesh generate(ChunkCoordinate coordinate, MeshSeq sequenceNumber);
}
