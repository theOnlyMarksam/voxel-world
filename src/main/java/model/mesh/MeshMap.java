package model.mesh;

import common.conversion.CoordinateConverter;
import model.coordinate.ChunkCoordinate;
import model.coordinate.MeshCoordinate;
import model.mesh.constants.MeshSeq;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector3f;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class MeshMap {
  private static final Logger LOG = LogManager.getFormatterLogger(MeshMap.class);

  private final Map<MeshCoordinate, Mesh> meshMap;

  public MeshMap() {
    this.meshMap = new HashMap<>();
  }

  /**
   * Reduces map to the given size.
   *
   * @param finalSize size after reduction
   * @param playerPos position of player
   * @return removed mesh coordinates
   */
  public List<MeshCoordinate> reduceSizeTo(int finalSize, Vector3f playerPos) {
    if (finalSize < meshMap.size()) {
      ChunkCoordinate center = CoordinateConverter.globalToChunk(playerPos);
      int reduceCount = meshMap.size() - finalSize;

      List<MeshCoordinate> removedElements =
          meshMap.keySet().stream()
              .sorted(Comparator.comparingDouble(center::distanceInChunkUnits))
              .skip(meshMap.size() - reduceCount)
              .collect(Collectors.toList());

      removedElements.forEach(meshMap::remove);
      LOG.debug("Removed %d elements from meshMap. Size is now %d", reduceCount, meshMap.size());

      return removedElements;
    }

    return Collections.emptyList();
  }

  /**
   * Reduces the size of the map by given amount.
   *
   * @param reduceCount number of meshes to remove
   * @param playerPos   position of player
   * @return removed mesh coordinates
   */
  public List<MeshCoordinate> reduceSizeBy(int reduceCount, Vector3f playerPos) {
    throw new UnsupportedOperationException("Not implemented");
  }

  public int getSize() {
    return meshMap.size();
  }

  public List<Mesh> getAllChangedMeshes() {
    return meshMap.values()
               .stream()
               .filter(Mesh::isChanged)
               .peek(m -> m.setHasChanged(false))
               .collect(Collectors.toList());
  }

  public void add(Mesh mesh) {
    meshMap.put(mesh.getPosition(), mesh);
    LOG.debug("Added %s to meshmap. Meshmap size is now %d", mesh.getPosition(), meshMap.size());
  }

  public Mesh get(MeshCoordinate meshCoordinate) {
    return meshMap.get(meshCoordinate);
  }

  public boolean contains(ChunkCoordinate chunkCoordinate) {
    return Arrays.stream(MeshSeq.values())
               .anyMatch(seq -> meshMap.containsKey(MeshCoordinate.of(chunkCoordinate, seq)));
  }

  public void shutdown() {
    meshMap.clear();
  }
}
