package model.mesh;

import files.textures.TextureMap;
import model.chunk.ChunkMap;
import model.coordinate.ChunkCoordinate;
import model.mesh.constants.MeshSeq;
import model.mesh.generator.MeshGenerator;
import model.mesh.generator.MeshGeneratorBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Mesher {
  private final Map<String, MeshGenerator> generatorMap;
  private final ChunkMap chunkMap;
  private final TextureMap textureMap;
  private ExecutorService executors;

  public Mesher(ChunkMap chunkMap, TextureMap textureMap) {
    this.generatorMap = new HashMap<>();
    this.chunkMap = chunkMap;
    this.textureMap = textureMap;
  }

  public void init() {
    this.executors = Executors.newSingleThreadExecutor();
  }

  public Future<Mesh> generateMesh(ChunkCoordinate coordinate,
                                   MeshSeq sequenceNr,
                                   String generatorName) {
    MeshGenerator generator = generatorMap.get(generatorName);
    ChunkCoordinate coordinateCopy = ChunkCoordinate.of(coordinate);

    return executors.submit(() -> generator.generate(coordinateCopy, sequenceNr));
  }

  public Mesh generateMeshNow(ChunkCoordinate coordinate,
                              MeshSeq sequenceNr,
                              String generatorName) {
    MeshGenerator generator = generatorMap.get(generatorName);
    ChunkCoordinate coordinateCopy = ChunkCoordinate.of(coordinate);

    return generator.generate(coordinateCopy, sequenceNr);
  }

  public void addMeshGenerator(String name, MeshGeneratorBuilder builder) {
    MeshGenerator generator = builder
                                  .chunkMap(chunkMap)
                                  .textureMap(textureMap)
                                  .build();
    generatorMap.put(name, generator);
  }

  public void shutdown() {
    executors.shutdownNow();
  }
}
