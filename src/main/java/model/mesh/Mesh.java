package model.mesh;

import model.coordinate.MeshCoordinate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Mesh {
  private static final Logger log = LogManager.getLogger(Mesh.class);

  private MeshCoordinate position;
  private VertexList vertexList = new VertexList();
  private boolean hasChanged = false;

  public Mesh(MeshCoordinate position) {
    this.position = position;
  }

  public VertexList getVertexList() {
    return vertexList;
  }

  public boolean isChanged() {
    return hasChanged;
  }

  public void setHasChanged(boolean hasChanged) {
    this.hasChanged = hasChanged;
  }

  public int getSize() {
    return vertexList.getVertexCount();
  }

  public MeshCoordinate getPosition() {
    return position;
  }

  public int getVertexCount() {
    return vertexList.getVertexCount();
  }
}
