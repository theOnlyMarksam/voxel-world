package model.mesh.constants;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum CubeFace {
  @JsonProperty("top")
  TOP(new float[]{
      // POS      // NORMAL   // UV
      // TR1
      0f, 1f, 0f, 0f, 1f, 0f, 1f, 1f, 0f,
      1f, 1f, 1f, 0f, 1f, 0f, 0f, 0f, 0f,
      1f, 1f, 0f, 0f, 1f, 0f, 0f, 1f, 0f,
      // TR2
      0f, 1f, 0f, 0f, 1f, 0f, 1f, 1f, 0f,
      0f, 1f, 1f, 0f, 1f, 0f, 1f, 0f, 0f,
      1f, 1f, 1f, 0f, 1f, 0f, 0f, 0f, 0f,
  }),

  @JsonProperty("front")
  FRONT(new float[]{
      // POS      // NORMAL    // UV
      // TR1
      0f, 0f, 0f, 0f, 0f, -1f, 1f, 1f, 0f,
      1f, 1f, 0f, 0f, 0f, -1f, 0f, 0f, 0f,
      1f, 0f, 0f, 0f, 0f, -1f, 0f, 1f, 0f,
      // TR2
      0f, 0f, 0f, 0f, 0f, -1f, 1f, 1f, 0f,
      0f, 1f, 0f, 0f, 0f, -1f, 1f, 0f, 0f,
      1f, 1f, 0f, 0f, 0f, -1f, 0f, 0f, 0f,
  }),

  @JsonProperty("left")
  LEFT(new float[]{
      // POS      // NORMAL   // UV
      // TR1
      1f, 0f, 0f, 1f, 0f, 0f, 1f, 1f, 0f,
      1f, 1f, 1f, 1f, 0f, 0f, 0f, 0f, 0f,
      1f, 0f, 1f, 1f, 0f, 0f, 0f, 1f, 0f,
      // TR2
      1f, 0f, 0f, 1f, 0f, 0f, 1f, 1f, 0f,
      1f, 1f, 0f, 1f, 0f, 0f, 1f, 0f, 0f,
      1f, 1f, 1f, 1f, 0f, 0f, 0f, 0f, 0f,
  }),

  @JsonProperty("right")
  RIGHT(new float[]{
      // POS       // NORMAL   // UV
      // TR1
      0f, 0f, 1f, -1f, 0f, 0f, 1f, 1f, 0f,
      0f, 1f, 0f, -1f, 0f, 0f, 0f, 0f, 0f,
      0f, 0f, 0f, -1f, 0f, 0f, 0f, 1f, 0f,
      // TR2
      0f, 0f, 1f, -1f, 0f, 0f, 1f, 1f, 0f,
      0f, 1f, 1f, -1f, 0f, 0f, 1f, 0f, 0f,
      0f, 1f, 0f, -1f, 0f, 0f, 0f, 0f, 0f,
  }),

  @JsonProperty("back")
  BACK(new float[]{
      // POS      // NORMAL   // UV
      // TR1
      1f, 0f, 1f, 0f, 0f, 1f, 1f, 1f, 0f,
      0f, 1f, 1f, 0f, 0f, 1f, 0f, 0f, 0f,
      0f, 0f, 1f, 0f, 0f, 1f, 0f, 1f, 0f,
      // TR2
      1f, 0f, 1f, 0f, 0f, 1f, 1f, 1f, 0f,
      1f, 1f, 1f, 0f, 0f, 1f, 1f, 0f, 0f,
      0f, 1f, 1f, 0f, 0f, 1f, 0f, 0f, 0f,
  }),

  @JsonProperty("bottom")
  BOTTOM(new float[]{
      // POS      // NORMAL    // UV
      // TR1
      0f, 0f, 1f, 0f, -1f, 0f, 1f, 1f, 0f,
      1f, 0f, 0f, 0f, -1f, 0f, 0f, 0f, 0f,
      1f, 0f, 1f, 0f, -1f, 0f, 0f, 1f, 0f,
      // TR2
      0f, 0f, 1f, 0f, -1f, 0f, 1f, 1f, 0f,
      0f, 0f, 0f, 0f, -1f, 0f, 1f, 0f, 0f,
      1f, 0f, 0f, 0f, -1f, 0f, 0f, 0f, 0f,
  });

  private float[] vertices;

  CubeFace(float[] vertices) {
    this.vertices = vertices;
  }

  public float[] getVertices() {
    return vertices;
  }
}
