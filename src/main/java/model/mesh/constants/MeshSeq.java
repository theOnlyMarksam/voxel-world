package model.mesh.constants;

public enum MeshSeq {
  LAND(0),
  WATER(1);

  private int seqNr;

  MeshSeq(int seqNr) {
    this.seqNr = seqNr;
  }

  public int getSeqNr() {
    return seqNr;
  }
}
