package model.mesh;

import model.coordinate.GlobalCoordinate;
import model.mesh.constants.CubeFace;
import org.joml.Vector3i;

public class VertexList {
  private static final int VERTEX_SIZE = 9;

  private int vertexCount;
  private float[] vertices;

  public VertexList() {
    vertexCount = 0;
    vertices = new float[VERTEX_SIZE];
  }

  @Deprecated
  public void add(CubeFace face, Vector3i cubeCoordinates, int textureIndex) {
    float[] vertices = face.getVertices();
    for (int i = 0; i < vertices.length; i += VERTEX_SIZE) {
      add(
          vertices[i] + cubeCoordinates.x()
          , vertices[i + 1] + cubeCoordinates.y()
          , vertices[i + 2] + cubeCoordinates.z()
          , vertices[i + 3]
          , vertices[i + 4]
          , vertices[i + 5]
          , vertices[i + 6]
          , vertices[i + 7]
          , textureIndex
      );
    }
  }

  public void add(CubeFace face, GlobalCoordinate cubeCoordinate, int textureIndex) {
    float[] vertices = face.getVertices();
    for (int i = 0; i < vertices.length; i += VERTEX_SIZE) {
      add(
          vertices[i] + cubeCoordinate.x()
          , vertices[i + 1] + cubeCoordinate.y()
          , vertices[i + 2] + cubeCoordinate.z()
          , vertices[i + 3]
          , vertices[i + 4]
          , vertices[i + 5]
          , vertices[i + 6]
          , vertices[i + 7]
          , textureIndex
      );
    }
  }

  public void add(float x, float y, float z, float nX, float nY, float nZ, float u, float v, float w) {
    if (vertices.length <= vertexCount * VERTEX_SIZE) {
      float[] tmp = new float[vertexCount * VERTEX_SIZE * 2];
      System.arraycopy(vertices, 0, tmp, 0, vertexCount * VERTEX_SIZE);
      vertices = tmp;
      push(x, y, z, nX, nY, nZ, u, v, w);
    } else {
      push(x, y, z, nX, nY, nZ, u, v, w);
    }
  }

  public float[] getDenseVertexArray() {
    float[] result = new float[vertexCount * VERTEX_SIZE];
    System.arraycopy(vertices, 0, result, 0, vertexCount * VERTEX_SIZE);
    return result;
  }

  public int getVertexCount() {
    return vertexCount;
  }

  /**
   * Sets {@link #vertexCount} to 0 and {@link #vertices}
   * to array of length {@link #VERTEX_SIZE}
   */
  public void clear() {
    vertexCount = 0;
    vertices = new float[VERTEX_SIZE];
  }

  private void push(float x, float y, float z, float nX, float nY, float nZ, float u, float v, float w) {
    vertices[vertexCount * VERTEX_SIZE] = x;
    vertices[vertexCount * VERTEX_SIZE + 1] = y;
    vertices[vertexCount * VERTEX_SIZE + 2] = z;
    vertices[vertexCount * VERTEX_SIZE + 3] = nX;
    vertices[vertexCount * VERTEX_SIZE + 4] = nY;
    vertices[vertexCount * VERTEX_SIZE + 5] = nZ;
    vertices[vertexCount * VERTEX_SIZE + 6] = u;
    vertices[vertexCount * VERTEX_SIZE + 7] = v;
    vertices[vertexCount * VERTEX_SIZE + 8] = w;
    vertexCount++;
  }
}
