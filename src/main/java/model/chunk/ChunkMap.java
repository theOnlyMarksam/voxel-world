package model.chunk;

import common.conversion.CoordinateConverter;
import model.block.BlockType;
import model.coordinate.ChunkCoordinate;
import model.coordinate.ChunkGlobalCoordinate;
import model.coordinate.GlobalCoordinate;
import model.landscape.LandscapeGenerator;
import model.landscape.impl.PerlinNoiseLandscapeGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector3f;

import java.util.HashMap;
import java.util.Map;

public class ChunkMap {
  private static final Logger LOG = LogManager.getLogger(ChunkMap.class);

  private final Map<ChunkCoordinate, Chunk> chunkMap;
  private final LandscapeGenerator landscapeGenerator;

  private Chunk lastChunk;
  private ChunkCoordinate lastCoordinate;

  public ChunkMap() {
    this.chunkMap = new HashMap<>();
    this.landscapeGenerator = new PerlinNoiseLandscapeGenerator();
    this.landscapeGenerator.init();
  }

  public BlockType front(GlobalCoordinate coordinate) {
    GlobalCoordinate f = GlobalCoordinate.of(coordinate.x(), coordinate.y(), coordinate.z() - 1);
    ChunkGlobalCoordinate c = CoordinateConverter.globalToChunkGlobal(f);
    Chunk chunk = getChunk(c);
    return chunk.getBlock(c.getChunkLocalCoordinate());
  }

  public BlockType behind(GlobalCoordinate coordinate) {
    GlobalCoordinate f = GlobalCoordinate.of(coordinate.x(), coordinate.y(), coordinate.z() + 1);
    ChunkGlobalCoordinate c = CoordinateConverter.globalToChunkGlobal(f);
    Chunk chunk = getChunk(c);
    return chunk.getBlock(c.getChunkLocalCoordinate());
  }

  public BlockType left(GlobalCoordinate coordinate) {
    GlobalCoordinate f = GlobalCoordinate.of(coordinate.x() + 1, coordinate.y(), coordinate.z());
    ChunkGlobalCoordinate c = CoordinateConverter.globalToChunkGlobal(f);
    Chunk chunk = getChunk(c);
    return chunk.getBlock(c.getChunkLocalCoordinate());
  }

  public BlockType right(GlobalCoordinate coordinate) {
    GlobalCoordinate f = GlobalCoordinate.of(coordinate.x() - 1, coordinate.y(), coordinate.z());
    ChunkGlobalCoordinate c = CoordinateConverter.globalToChunkGlobal(f);
    Chunk chunk = getChunk(c);
    return chunk.getBlock(c.getChunkLocalCoordinate());
  }

  public BlockType above(GlobalCoordinate coordinate) {
    ChunkGlobalCoordinate c = CoordinateConverter.globalToChunkGlobal(coordinate);
    Chunk chunk = getChunk(c);
    return chunk.above(c.getChunkLocalCoordinate());
  }

  public BlockType below(GlobalCoordinate coordinate) {
    ChunkGlobalCoordinate c = CoordinateConverter.globalToChunkGlobal(coordinate);
    Chunk chunk = getChunk(c);
    return chunk.below(c.getChunkLocalCoordinate());
  }

  private Chunk getChunk(ChunkGlobalCoordinate crd) {
    ChunkCoordinate c = crd.getChunkCoordinate();
    if (c.equals(lastCoordinate)) {
      return lastChunk;
    } else {
      Chunk chunk = chunkMap.get(c);
      lastCoordinate = c;

      if (chunk != null) {
        lastChunk = chunk;
        return chunk;
      }

      Chunk generated = generateChunk(c);

      lastCoordinate = c;
      lastChunk = generated;

      chunkMap.put(c, generated);
      return generated;
    }
  }

  private Chunk generateChunk(ChunkCoordinate chunkCoordinate) {
    Chunk c = new Chunk(chunkCoordinate);
    landscapeGenerator.generate(c);
    return c;
  }

  public BlockType getBlock(Vector3f globalCoordinate) {
    ChunkGlobalCoordinate c = CoordinateConverter.vectorToChunkGlobal(globalCoordinate);
    return getBlock(c);
  }

  public BlockType getBlock(GlobalCoordinate coordinate) {
    ChunkGlobalCoordinate c = CoordinateConverter.globalToChunkGlobal(coordinate);
    return getBlock(c);
  }

  public BlockType getBlock(ChunkGlobalCoordinate chunkGlobalCoordinate) {
    Chunk chunk = getChunk(chunkGlobalCoordinate);
    return chunk.getBlock(chunkGlobalCoordinate.getChunkLocalCoordinate());
  }

  public void setBlock(Vector3f globalCoordinate, BlockType blockType) {
    ChunkGlobalCoordinate c = CoordinateConverter.vectorToChunkGlobal(globalCoordinate);
    setBlock(c, blockType);
  }

  public void setBlock(GlobalCoordinate globalCoordinate, BlockType blockType) {
    ChunkGlobalCoordinate c = CoordinateConverter.globalToChunkGlobal(globalCoordinate);
    setBlock(c, blockType);
  }

  public void setBlock(ChunkGlobalCoordinate chunkGlobalCoordinate, BlockType blockType) {
    Chunk chunk = chunkMap.computeIfAbsent(chunkGlobalCoordinate.getChunkCoordinate(), this::generateChunk);
    chunk.setBlock(chunkGlobalCoordinate.getChunkLocalCoordinate(), blockType);
  }
}
