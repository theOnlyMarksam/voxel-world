package model.chunk;

import model.biome.BiomeType;
import model.block.BlockType;
import model.coordinate.ChunkCoordinate;
import model.coordinate.ChunkLocalCoordinate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Chunk {
  private static final Logger log = LogManager.getLogger(Chunk.class);
  public static final int SIDE_LENGTH = 16;
  public static final int HEIGHT = 256;
  public static final int LAYER_SIZE = SIDE_LENGTH * SIDE_LENGTH;
  public static final int CHUNK_BLOCK_COUNT = LAYER_SIZE * HEIGHT;

  private final BlockType[] blocks = new BlockType[CHUNK_BLOCK_COUNT];
  private final BiomeType[] biomeTypes = new BiomeType[LAYER_SIZE];
  private final ChunkCoordinate position;

  public Chunk(ChunkCoordinate coordinate) {
    this.position = coordinate;

    for (int i = 0; i < blocks.length; i++) {
      blocks[i] = BlockType.AIR;
    }
  }

  public BlockType[] getAllBlocks() {
    return blocks;
  }

  public ChunkCoordinate getPosition() {
    return position;
  }

  public int x() {
    return position.x();
  }

  public int z() {
    return position.z();
  }

  public void setBiomeType(ChunkLocalCoordinate coordinate, BiomeType biomeType) {
    biomeTypes[coordinate.getColumnIndex()] = biomeType;
  }

  public BiomeType getBiomeType(ChunkLocalCoordinate coordinate) {
    return biomeTypes[coordinate.getColumnIndex()];
  }

  public void setBlock(ChunkLocalCoordinate coordinate, BlockType type) {
    blocks[coordinate.getIndex()] = type;
  }

  public BlockType getBlock(ChunkLocalCoordinate coordinate) {
    return blocks[coordinate.getIndex()];
  }

  public BlockType above(ChunkLocalCoordinate chunkLocalCoordinate) {
    if (chunkLocalCoordinate.y() >= Chunk.HEIGHT - 1) {
      return BlockType.AIR;
    }
    return blocks[chunkLocalCoordinate.getIndex() + Chunk.LAYER_SIZE];
  }

  public BlockType below(ChunkLocalCoordinate chunkLocalCoordinate) {
    if (chunkLocalCoordinate.y() <= 0) {
      return BlockType.AIR;
    }
    return blocks[chunkLocalCoordinate.getIndex() - Chunk.LAYER_SIZE];
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Chunk chunk = (Chunk) o;

    return position.equals(chunk.position);
  }

  @Override
  public int hashCode() {
    int result = position.x();
    result = 31 * result + position.z();
    return result;
  }

  @Override
  public String toString() {
    return "Chunk{" +
               "position=" + position +
               '}';
  }
}
