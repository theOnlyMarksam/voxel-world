package model.block;

import model.mesh.constants.CubeFace;

import java.util.Map;

public class BlockInfo {
  private Map<BlockType, Block> blockMap;

  public String getTextureName(BlockType blockType, CubeFace face) {
    return blockMap.get(blockType).getTextureName(face);
  }

  public Map<BlockType, Block> getBlockMap() {
    return blockMap;
  }

  public void setBlockMap(Map<BlockType, Block> blockMap) {
    this.blockMap = blockMap;
  }
}
