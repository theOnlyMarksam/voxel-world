package model.block;

import model.mesh.constants.CubeFace;

import java.util.Map;

public class Block {
  private BlockType type;
  private Map<CubeFace, String> textures;

  public String getTextureName(CubeFace face) {
    return textures.get(face);
  }

  public BlockType getType() {
    return type;
  }

  public void setType(BlockType type) {
    this.type = type;
  }

  public Map<CubeFace, String> getTextures() {
    return textures;
  }

  public void setTextures(Map<CubeFace, String> textures) {
    this.textures = textures;
  }
}
