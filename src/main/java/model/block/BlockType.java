package model.block;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum BlockType {
  @JsonProperty("air")
  AIR,
  @JsonProperty("dirt")
  DIRT,
  @JsonProperty("grass")
  GRASS,
  @JsonProperty("sand")
  SAND,
  @JsonProperty("stone")
  STONE,
  @JsonProperty("water")
  WATER
}
