package files.font.bitmap;

import files.textures.Image;

public class FontPage {
  private int id;
  private String name;
  private Image bitmap;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Image getBitmap() {
    return bitmap;
  }

  public void setBitmap(Image bitmap) {
    this.bitmap = bitmap;
  }
}
