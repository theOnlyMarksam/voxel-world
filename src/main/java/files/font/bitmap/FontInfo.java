package files.font.bitmap;

import org.joml.Vector2i;
import org.joml.Vector4i;

public class FontInfo {
  private String face;
  private int size;
  private boolean bold;
  private boolean italic;
  private String charset;
  private boolean unicode;
  private int stretchH;
  private boolean smooth;
  private int aa;
  private Vector4i padding;
  private Vector2i spacing;
  private int outline;

  public String getFace() {
    return face;
  }

  public void setFace(String face) {
    this.face = face;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public boolean isBold() {
    return bold;
  }

  public void setBold(boolean bold) {
    this.bold = bold;
  }

  public boolean isItalic() {
    return italic;
  }

  public void setItalic(boolean italic) {
    this.italic = italic;
  }

  public String getCharset() {
    return charset;
  }

  public void setCharset(String charset) {
    this.charset = charset;
  }

  public boolean isUnicode() {
    return unicode;
  }

  public void setUnicode(boolean unicode) {
    this.unicode = unicode;
  }

  public int getStretchH() {
    return stretchH;
  }

  public void setStretchH(int stretchH) {
    this.stretchH = stretchH;
  }

  public boolean isSmooth() {
    return smooth;
  }

  public void setSmooth(boolean smooth) {
    this.smooth = smooth;
  }

  public int getAa() {
    return aa;
  }

  public void setAa(int aa) {
    this.aa = aa;
  }

  public Vector4i getPadding() {
    return padding;
  }

  public void setPadding(Vector4i padding) {
    this.padding = padding;
  }

  public Vector2i getSpacing() {
    return spacing;
  }

  public void setSpacing(Vector2i spacing) {
    this.spacing = spacing;
  }

  public int getOutline() {
    return outline;
  }

  public void setOutline(int outline) {
    this.outline = outline;
  }
}
