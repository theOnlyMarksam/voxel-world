package files.font.bitmap;

import java.util.Map;

public class FontChar {
  private int id;
  private int x;
  private int y;
  private int width;
  private int height;
  private int xOffset;
  private int yOffset;
  private int xAdvance;
  private int page;
  private StoreType chnl;
  private Map<Integer, Integer> kerningMap;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public int getxOffset() {
    return xOffset;
  }

  public void setxOffset(int xOffset) {
    this.xOffset = xOffset;
  }

  public int getyOffset() {
    return yOffset;
  }

  public void setyOffset(int yOffset) {
    this.yOffset = yOffset;
  }

  public int getxAdvance() {
    return xAdvance;
  }

  public void setxAdvance(int xAdvance) {
    this.xAdvance = xAdvance;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public StoreType getChnl() {
    return chnl;
  }

  public void setChnl(StoreType chnl) {
    this.chnl = chnl;
  }

  public Map<Integer, Integer> getKerningMap() {
    return kerningMap;
  }

  public void setKerningMap(Map<Integer, Integer> kerningMap) {
    this.kerningMap = kerningMap;
  }

  enum StoreType {
    BLUE,
    GREEN,
    RED,
    ALPHA,
    ALL
  }
}
