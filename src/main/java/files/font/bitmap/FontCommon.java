package files.font.bitmap;

public class FontCommon {
  private int lineHeight;
  private int base;
  private int scaleW;
  private int scaleH;
  private int pages;
  private boolean packed;
  private PackType alphaChnl;
  private PackType redChnl;
  private PackType greenChnl;
  private PackType blueChnl;

  public int getLineHeight() {
    return lineHeight;
  }

  public void setLineHeight(int lineHeight) {
    this.lineHeight = lineHeight;
  }

  public int getBase() {
    return base;
  }

  public void setBase(int base) {
    this.base = base;
  }

  public int getScaleW() {
    return scaleW;
  }

  public void setScaleW(int scaleW) {
    this.scaleW = scaleW;
  }

  public int getScaleH() {
    return scaleH;
  }

  public void setScaleH(int scaleH) {
    this.scaleH = scaleH;
  }

  public int getPages() {
    return pages;
  }

  public void setPages(int pages) {
    this.pages = pages;
  }

  public boolean isPacked() {
    return packed;
  }

  public void setPacked(boolean packed) {
    this.packed = packed;
  }

  public PackType getAlphaChnl() {
    return alphaChnl;
  }

  public void setAlphaChnl(PackType alphaChnl) {
    this.alphaChnl = alphaChnl;
  }

  public PackType getRedChnl() {
    return redChnl;
  }

  public void setRedChnl(PackType redChnl) {
    this.redChnl = redChnl;
  }

  public PackType getGreenChnl() {
    return greenChnl;
  }

  public void setGreenChnl(PackType greenChnl) {
    this.greenChnl = greenChnl;
  }

  public PackType getBlueChnl() {
    return blueChnl;
  }

  public void setBlueChnl(PackType blueChnl) {
    this.blueChnl = blueChnl;
  }

  enum PackType {
    OUTLINE,
    GLYPH,
    OUTLINE_AND_GLYPH
  }
}
