package files.font.bitmap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector2i;
import org.joml.Vector4i;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class FontReader {
  private static final Logger LOG = LogManager.getFormatterLogger(FontReader.class);

  public static Font readFont(String fontFile) {
    Font font = new Font();
    try (LineReader reader = new LineReader(fontFile)) {
      font.setFontInfo(readFontInfo(reader.readLine()));
      font.setFontCommon(readFontCommon(reader.readLine()));
      font.setFontPages(readFontPages(reader, font.getFontCommon().getPages()));
      font.setFontCharMap(readChars(reader));

      if (reader.readLine() != null) {
        throw new RuntimeException("File should have ended");
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return font;
  }

  private static FontInfo readFontInfo(Line line) {
    if (line.lineType != LineType.INFO) {
      throw new IllegalArgumentException("Line should begin with 'info' keyword");
    }

    FontInfo fontInfo = new FontInfo();

    fontInfo.setFace(line.getString("face"));
    fontInfo.setSize(line.getInt("size"));
    fontInfo.setBold(line.getBoolean("bold"));
    fontInfo.setItalic(line.getBoolean("italic"));
    fontInfo.setCharset(line.getString("charset"));
    fontInfo.setUnicode(line.getBoolean("unicode"));
    fontInfo.setStretchH(line.getInt("stretchH"));
    fontInfo.setSmooth(line.getBoolean("smooth"));
    fontInfo.setAa(line.getInt("aa"));
    fontInfo.setPadding(line.getVec4i("padding"));
    fontInfo.setSpacing(line.getVec2i("spacing"));
    fontInfo.setOutline(line.getInt("outline"));

    return fontInfo;
  }

  private static FontCommon readFontCommon(Line line) {
    if (line.lineType != LineType.COMMON) {
      throw new IllegalArgumentException("Line should begin with 'common' keyword");
    }

    FontCommon fontCommon = new FontCommon();

    fontCommon.setLineHeight(line.getInt("lineHeight"));
    fontCommon.setBase(line.getInt("base"));
    fontCommon.setScaleW(line.getInt("scaleW"));
    fontCommon.setScaleH(line.getInt("scaleH"));
    fontCommon.setPages(line.getInt("pages"));
    fontCommon.setPacked(line.getBoolean("packed"));
    fontCommon.setAlphaChnl(line.getPackType("alphaChnl"));
    fontCommon.setRedChnl(line.getPackType("redChnl"));
    fontCommon.setGreenChnl(line.getPackType("greenChnl"));
    fontCommon.setBlueChnl(line.getPackType("blueChnl"));

    return fontCommon;
  }

  private static FontPage[] readFontPages(LineReader reader, int pages) throws IOException {
    FontPage[] fontPages = new FontPage[pages];

    for (int i = 0; i < pages; i++) {
      Line line = reader.readLine();
      fontPages[i] = createFontPage(line);
    }

    return fontPages;
  }

  private static FontPage createFontPage(Line line) {
    if (line.lineType != LineType.PAGE) {
      throw new IllegalArgumentException("Line should begin with 'page' keyword");
    }

    FontPage page = new FontPage();

    page.setId(line.getInt("id"));
    page.setName(line.getString("file"));

    return page;
  }

  private static Map<Integer, FontChar> readChars(LineReader reader) throws IOException {
    Line line = reader.readLine();

    if (line.lineType != LineType.CHARS) {
      throw new IllegalArgumentException("Line should begin with 'chars' keyword");
    }

    Map<Integer, FontChar> fontCharMap = new HashMap<>();
    int charCount = line.getInt("count");

    for (int i = 0; i < charCount; i++) {
      line = reader.readLine();
      FontChar fontChar = readFontChar(line);
      fontChar.setKerningMap(new HashMap<>());
      fontCharMap.put(fontChar.getId(), fontChar);
    }

    line = reader.readLine();

    if (line.lineType != LineType.KERNINGS) {
      throw new IllegalArgumentException("Line should begin with 'kernings' keyword");
    }

    int kerningCount = line.getInt("count");
    for (int i = 0; i < kerningCount; i++) {
      line = reader.readLine();

      if (line.lineType != LineType.KERNING) {
        throw new IllegalArgumentException("Line should begin with 'kerning' keyword");
      }

      FontChar fontChar = fontCharMap.get(line.getInt("first"));
      fontChar.getKerningMap().put(line.getInt("second"), line.getInt("amount"));
    }

    return fontCharMap;
  }

  private static FontChar readFontChar(Line line) {
    if (line.lineType != LineType.CHAR) {
      throw new IllegalArgumentException("Line should begin with 'char' keyword");
    }

    FontChar fontChar = new FontChar();

    fontChar.setId(line.getInt("id"));
    fontChar.setX(line.getInt("x"));
    fontChar.setY(line.getInt("y"));
    fontChar.setWidth(line.getInt("width"));
    fontChar.setHeight(line.getInt("height"));
    fontChar.setxOffset(line.getInt("xoffset"));
    fontChar.setyOffset(line.getInt("yoffset"));
    fontChar.setxAdvance(line.getInt("xadvance"));
    fontChar.setPage(line.getInt("page"));
    fontChar.setChnl(line.getStoreType("chnl"));

    return fontChar;
  }

  private static class LineReader implements AutoCloseable {
    private InputStream inputStream;
    private String fontFileName;
    private byte[] buffer = new byte[1024];
    private int offset = 0;
    private int total = 0;
    private LineBuffer lineBuffer = new LineBuffer();
    private boolean hasBytes = true;
    private boolean completed = false;

    LineReader(String fontFileName) {
      this.inputStream = FontReader.class.getResourceAsStream(fontFileName);
      this.fontFileName = fontFileName;
    }

    Line readLine() throws IOException {
      if (completed) {
        return null;
      }

      while (hasBytes) {
        int numRead = inputStream.read(buffer, offset, buffer.length - offset);
        total += numRead;

        if (numRead == -1) {
          hasBytes = false;
          break;
        }

        int lineEndingIndex = searchLineEnding(buffer, offset + numRead);

        if (lineEndingIndex != -1) {
          lineBuffer.add(buffer, lineEndingIndex + 1);
          String fileRow = lineBuffer.getString();

          System.arraycopy(buffer, lineEndingIndex + 1, buffer, 0, buffer.length - lineEndingIndex - 1);
          offset = offset + numRead - lineEndingIndex - 1;

          return convertToLine(fileRow);
        } else {
          lineBuffer.add(buffer, numRead + offset);
          offset = 0;
        }
      }

      int lineEndingIndex = searchLineEnding(buffer, offset);

      if (lineEndingIndex == -1) {
        lineBuffer.add(buffer, offset);
        completed = true;
        return convertToLine(lineBuffer.getString());
      } else {
        lineBuffer.add(buffer, lineEndingIndex + 1);
        String fileRow = lineBuffer.getString();

        System.arraycopy(buffer, lineEndingIndex + 1, buffer, 0, offset - lineEndingIndex + 1);
        offset -= (lineEndingIndex + 1);

        if (offset == 0) {
          completed = true;
        }

        return convertToLine(fileRow);
      }
    }

    private static Line convertToLine(String fileRow) {
      Line line = new Line();
      line.params = new HashMap<>();
      String[] rowParts = fileRow.trim().split("\\s+");

      line.lineType = Arrays
                          .stream(LineType.values())
                          .filter(type -> type.getTextForm().equals(rowParts[0]))
                          .findFirst()
                          .orElse(null);

      for (int i = 1; i < rowParts.length; i++) {
        String[] param = rowParts[i].split("=");
        line.params.put(param[0], param[1]);
      }

      return line;
    }

    private static int searchLineEnding(byte[] buffer, int numRead) {
      for (int i = 0; i < numRead; i++) {
        if (buffer[i] == '\n') {
          return i;
        }
      }
      return -1;
    }

    @Override
    public void close() throws IOException {
      LOG.debug("Read %d bytes from %s", total, fontFileName);
      inputStream.close();
    }

    @Override
    public String toString() {
      return new String(buffer);
    }
  }

  private static class Line {
    LineType lineType;
    Map<String, String> params;

    String getString(String name) {
      String p = params.get(name);
      if (p.startsWith("\"") && p.endsWith("\"")) {
        return p.substring(1, p.length() - 1);
      }
      throw new IllegalArgumentException("This parameter is not of string type");
    }

    int getInt(String name) {
      return Integer.parseInt(params.get(name));
    }

    boolean getBoolean(String name) {
      return "1".equals(params.get(name));
    }

    Vector2i getVec2i(String name) {
      String[] parts = params.get(name).split(",");

      if (parts.length != 2)
        throw new IllegalArgumentException("This parameter cannot be converted to vec 2");

      return new Vector2i(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
    }

    Vector4i getVec4i(String name) {
      String[] parts = params.get(name).split(",");

      if (parts.length != 4)
        throw new IllegalArgumentException("This parameter cannot be converted to vec 2");

      return new Vector4i(
          Integer.parseInt(parts[0]),
          Integer.parseInt(parts[1]),
          Integer.parseInt(parts[2]),
          Integer.parseInt(parts[3])
      );
    }

    FontCommon.PackType getPackType(String name) {
      int type = getInt(name);

      switch (type) {
        case 0:
        case 3:
          return FontCommon.PackType.GLYPH;
        case 1:
        case 4:
          return FontCommon.PackType.OUTLINE;
        case 2:
          return FontCommon.PackType.OUTLINE_AND_GLYPH;
        default:
          throw new IllegalArgumentException("Wrong pack type: " + type);
      }
    }

    FontChar.StoreType getStoreType(String name) {
      int type = getInt(name);

      switch (type) {
        case 1:
          return FontChar.StoreType.BLUE;
        case 2:
          return FontChar.StoreType.GREEN;
        case 4:
          return FontChar.StoreType.RED;
        case 8:
          return FontChar.StoreType.ALPHA;
        case 15:
          return FontChar.StoreType.ALL;
        default:
          throw new IllegalArgumentException("Wrong store type: " + type);
      }
    }
  }

  private enum LineType {
    INFO("info"),
    COMMON("common"),
    PAGE("page"),
    CHARS("chars"),
    CHAR("char"),
    KERNINGS("kernings"),
    KERNING("kerning");

    private String textForm;

    LineType(String textForm) {
      this.textForm = textForm;
    }

    public String getTextForm() {
      return textForm;
    }
  }

  private static class LineBuffer {
    byte[] buffer;
    int position;

    void add(byte[] b, int numToAdd) {
      if (buffer == null) {
        buffer = new byte[numToAdd * 2];
        System.arraycopy(b, 0, buffer, 0, numToAdd);
        position = numToAdd;
      } else if (buffer.length <= position + numToAdd) {
        byte[] tmp = new byte[(position + numToAdd) * 2];
        System.arraycopy(buffer, 0, tmp, 0, position);
        buffer = tmp;
        System.arraycopy(b, 0, buffer, position, numToAdd);
        position += numToAdd;
      } else {
        System.arraycopy(b, 0, buffer, position, numToAdd);
        position += numToAdd;
      }
    }

    void clear() {
      position = 0;
      buffer = null;
    }

    String getString() {
      if (position == 0) return null;

      String stringValue = new String(buffer, 0, position);
      clear();
      return stringValue;
    }

    @Override
    public String toString() {
      return position > 0 ? new String(buffer, 0, position) : null;
    }
  }
}
