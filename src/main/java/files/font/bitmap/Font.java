package files.font.bitmap;

import java.util.Map;

public class Font {
  private final int defaultCharName = 35;
  private FontInfo fontInfo;
  private FontCommon fontCommon;
  private FontPage[] fontPages;
  private Map<Integer, FontChar> fontCharMap;
  private FontChar defaultChar;

  public FontInfo getFontInfo() {
    return fontInfo;
  }

  public void setFontInfo(FontInfo fontInfo) {
    this.fontInfo = fontInfo;
  }

  public FontCommon getFontCommon() {
    return fontCommon;
  }

  public void setFontCommon(FontCommon fontCommon) {
    this.fontCommon = fontCommon;
  }

  public FontPage[] getFontPages() {
    return fontPages;
  }

  public void setFontPages(FontPage[] fontPages) {
    this.fontPages = fontPages;
  }

  public Map<Integer, FontChar> getFontCharMap() {
    return fontCharMap;
  }

  public void setFontCharMap(Map<Integer, FontChar> fontCharMap) {
    this.fontCharMap = fontCharMap;
    this.defaultChar = fontCharMap.get(defaultCharName);
  }

  public FontChar getChar(char name) {
    return fontCharMap.getOrDefault((int) name, defaultChar);
  }
}
