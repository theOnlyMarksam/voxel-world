package files.font.bitmap;

public class FontKerning {
  private int first;
  private int second;

  /**
   * How much the x position should be adjusted when drawing the second character immediately following the first.
   */
  private int amount;

  public int getFirst() {
    return first;
  }

  public void setFirst(int first) {
    this.first = first;
  }

  public int getSecond() {
    return second;
  }

  public void setSecond(int second) {
    this.second = second;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }
}
