package files.textures;

import java.nio.ByteBuffer;

public class Image {
  private int width;
  private int height;
  private Channels channels;
  private String fileName;
  private ByteBuffer image;

  public Image(String fileName) {
    this.fileName = fileName;
  }

  public void load() {
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public void setChannels(Channels channels) {
    this.channels = channels;
  }

  public void setImage(ByteBuffer image) {
    this.image = image;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public Channels getChannels() {
    return channels;
  }

  public String getFileName() {
    return fileName;
  }

  public ByteBuffer getImage() {
    return image;
  }
}
