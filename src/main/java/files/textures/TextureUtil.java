package files.textures;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.BufferUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

public class TextureUtil {
  private static final Logger log = LogManager.getLogger(TextureUtil.class);

  /**
   * Loads image file from classpath relative to /textures. Meaning that if argument
   * is test.png it is loaded from classpath as /textures/test.png
   * @param fileName name of the image file
   * @param channels contains the required channels and their order for example 'RGBA' or 'RGB'
   */
  public static Image loadImage(String fileName, Channels channels) {
    String imagePath = "/textures/" + fileName;
    Image image = new Image(fileName);
    image.setChannels(channels);
    BufferedImage buffer;

    try {
      buffer = ImageIO.read(TextureUtil.class.getResourceAsStream(imagePath));
    } catch (IOException ioEx) {
      throw new RuntimeException(ioEx);
    }

    image.setWidth(buffer.getWidth());
    image.setHeight(buffer.getHeight());
    int pixelCount = buffer.getWidth() * buffer.getHeight();
    ByteBuffer bytes = BufferUtils.createByteBuffer(image.getHeight() * image.getWidth() * channels.toString().length());

    for (int i = 0; i < pixelCount; i++) {
      int y = i / image.getWidth();
      int x = i % image.getWidth();

      putPixelValue(buffer.getRGB(x, y), bytes, channels.toString());
    }

    bytes.flip();
    image.setImage(bytes);
    return image;
  }

  private static void putPixelValue(int pixel, ByteBuffer bytes, String channels) {
    for (int i = 0; i < channels.length(); i++) {
      switch (channels.charAt(i)) {
        case 'R':
          bytes.put(getR(pixel));
          continue;
        case 'G':
          bytes.put(getG(pixel));
          continue;
        case 'B':
          bytes.put(getB(pixel));
          continue;
        case 'A':
          bytes.put(getA(pixel));
          continue;
        default:
          throw new IllegalArgumentException("Wrong channel name: " + channels.charAt(i));
      }
    }
  }

  private static byte getA(int pixel) {
    return (byte) ((pixel >> 24) & 0xFF);
  }

  private static byte getR(int pixel) {
    return (byte) ((pixel >> 16) & 0xFF);
  }

  private static byte getG(int pixel) {
    return (byte) ((pixel >> 8) & 0xFF);
  }

  private static byte getB(int pixel) {
    return (byte) (pixel & 0xFF);
  }
}
