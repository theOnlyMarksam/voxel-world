package files.textures;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Channels {
  @JsonProperty("rgb")
  RGB,
  @JsonProperty("rgba")
  RGBA
}
