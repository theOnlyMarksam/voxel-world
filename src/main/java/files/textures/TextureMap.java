package files.textures;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.block.BlockInfo;
import model.block.BlockType;
import model.mesh.constants.CubeFace;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.BufferUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TextureMap {
  private static final Logger LOG = LogManager.getLogger(TextureMap.class);
  private List<Image> textures = new ArrayList<>();
  private Map<String, Integer> textureIndices = new HashMap<>();
  private BlockInfo blockInfo;
  private boolean changed = false;

  public void initTextureMap() {
    initBlockInfo();
  }

  private void initBlockInfo() {
    ObjectMapper om = new ObjectMapper();
    try {
      blockInfo = om.readValue(
          getClass().getResourceAsStream("/properties/blocks/blocks.json"),
          BlockInfo.class
      );
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public BlockInfo getBlockInfo() {
    return blockInfo;
  }

  /**
   * Loads the texture if it is not already present, otherwise just returns the
   * texture index.
   *
   * @return Index of the texture in this map.
   */
  public int loadAndGetIndex(BlockType blockType, CubeFace face) {
    String textureName = blockInfo.getTextureName(blockType, face);

    if (textureIndices.containsKey(textureName)) {
      return textureIndices.get(textureName);
    }

    return loadTexture(textureName, Channels.RGBA);
  }

  /**
   * @return index of texture or -1 if no such texture is loaded
   */
  public int getTextureIndex(BlockType blockType, CubeFace face) {
    String textureName = blockInfo.getTextureName(blockType, face);
    return textureIndices.getOrDefault(textureName, -1);
  }

  private int loadTexture(String textureName, Channels channels) {
    Image image = TextureUtil.loadImage(textureName, channels);
    int nextIndex = textures.size();
    textureIndices.put(textureName, nextIndex);
    textures.add(image);

    changed = true;

    return nextIndex;
  }

  public int getLayerCount() {
    return textures.size();
  }

  public ByteBuffer getBytes() {
    int totalByteCount = 0;
    for (Image texture : textures) {
      totalByteCount += texture.getImage().capacity();
    }

    ByteBuffer imageBytes = BufferUtils.createByteBuffer(totalByteCount);
    textures.forEach(img -> {
      img.getImage().rewind();
      imageBytes.put(img.getImage());
    });
    imageBytes.flip();
    changed = false;

    return imageBytes;
  }

  public boolean isChanged() {
    return changed;
  }
}
