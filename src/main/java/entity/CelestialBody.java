package entity;

import files.textures.Channels;
import files.textures.Image;
import files.textures.TextureUtil;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;

public class CelestialBody {

  private static final float DISTANCE_FROM_PLAYER = 345;
  private float rotation = (float) Math.PI / 4f;
  private Vector3f rotationAxis = new Vector3f(1f, 0f, 0f);

  private Image texture;
  private int textureId;
  private Player player;
  private Matrix4f model;

  private Vector4f intermediatePos = new Vector4f();
  private Vector3f pos = new Vector3f();

  public CelestialBody(String textureName, Player player) {
    texture = TextureUtil.loadImage("entities/celestial/" + textureName, Channels.RGB);
    this.player = player;

    model = new Matrix4f()
                .translation(player.getPosition().x, player.getPosition().y, player.getPosition().z)
                .rotate(rotation, rotationAxis)
                .translate(0, -DISTANCE_FROM_PLAYER, 0);
  }

  public void update(float dt) {
    model = model
                .translation(player.getPosition().x, player.getPosition().y, player.getPosition().z)
                .rotate(rotation, rotationAxis)
                .translate(0, -DISTANCE_FROM_PLAYER, 0);
    rotation -= dt * 0.0052399; // Math.PI / 600
  }

  public void addRotation(float rotation) {
    this.rotation = (float) Math.PI / 4f + rotation;
    model = model
                .translation(player.getPosition().x, player.getPosition().y, player.getPosition().z)
                .rotate(this.rotation, rotationAxis).scale(20)
                .translate(-0.5f, -10f, -0.5f);
  }

  public Image getTexture() {
    return texture;
  }

  public Matrix4f getModelMatrix() {
    return model;
  }

  public void setTextureId(int textureId) {
    this.textureId = textureId;
  }

  public int getTextureId() {
    return textureId;
  }

  public Vector3f getPosition() {
    intermediatePos = intermediatePos.set(0, 0, 0, 1).mul(model);

    return pos.set(intermediatePos.x, intermediatePos.y, intermediatePos.z).div(intermediatePos.w);
  }
}
