package main;

import files.font.bitmap.Font;

public class ApplicationContext {
  private int chunkLength;
  private int chunkHeight;
  private int layerSize;
  private int chunkBlockCount;
  private int visibleChunkRadius;
  private int renderDistance;
  private float blockModifyDistance;
  private long blockBreakTimeLimit;
  private static Font font;

  private ApplicationContext() {}

  public static Font getFont() {
    return font;
  }

  public static void setFont(Font f) {
    font = f;
  }
}
