package shader;

public enum ShaderAttrib {
  UV("uv"),
  NORMAL("normal"),
  POSITION("position");

  private String name;

  ShaderAttrib(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
