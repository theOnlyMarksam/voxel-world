package shader;

public enum ShaderUniform {
  VIEW("view"),
  PROJECTION("projection"),
  MODEL("model"),
  LIGHT_POS("lightPos"),
  MVP_MATRIX("mvpMatrix");

  private final String name;

  ShaderUniform(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
