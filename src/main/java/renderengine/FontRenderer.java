package renderengine;

import files.textures.Image;
import main.ApplicationContext;
import main.Game;
import mechanics.element.GuiElementMesh;
import mechanics.element.Rectangle;
import model.gui.text.GuiText;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import shader.Shader;

import java.util.ArrayList;
import java.util.List;

public class FontRenderer {
  private List<GuiRenderTarget> renderTargets = new ArrayList<>();
  private Shader shader = new Shader("font");
  private int fontTexture;
  private Vector3f textColor = new Vector3f(1.0f, 1.0f, 1.0f);

  public FontRenderer() {
    fontTexture = initFontTexture();
  }

  private int initFontTexture() {
    Image image = ApplicationContext.getFont().getFontPages()[0].getBitmap();
    int texture = GL11.glGenTextures();
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

    GL11.glTexImage2D(
        GL11.GL_TEXTURE_2D,
        0,
        GL11.GL_RGB,
        image.getWidth(),
        image.getHeight(),
        0,
        GL11.GL_RGB,
        GL11.GL_UNSIGNED_BYTE,
        image.getImage()
    );

    return texture;
  }

  public void render() {
    GL11.glDisable(GL11.GL_DEPTH_TEST);
    GL20.glUseProgram(shader.getProgram());
    GL11.glEnable(GL11.GL_BLEND);
    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

    GL13.glActiveTexture(GL13.GL_TEXTURE0);
    shader.uniform1i("fontTexture", 0);
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, fontTexture);
    shader.uniform3f("textColor", textColor);

    for (GuiRenderTarget target : renderTargets) {
      GL30.glBindVertexArray(target.getVao());
      GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, target.indexBuffer());
      shader.uniformMatrix4fv("transformationMatrix", target.transform);
      GL11.glDrawElements(GL11.GL_TRIANGLES, target.getSize(), GL11.GL_UNSIGNED_INT, 0);
    }

    GL20.glUseProgram(0);
    GL11.glEnable(GL11.GL_DEPTH_TEST);
    GL11.glDisable(GL11.GL_BLEND);
  }

  public void bindText(GuiText text) {
    Vao vao = createVao(text.getMesh());
    Matrix4f transform = createTransform(text);
    renderTargets.add(new GuiRenderTarget(transform, 0, vao));
  }

  private Matrix4f createTransform(GuiText text) {
    Rectangle container = text.getContainer();

    float containerCenterX = (container.getX() + container.getWidth() / 2f) / (float) Game.WIDTH * 2f - 1f;
    float containerCenterY = (Game.HEIGHT - (container.getY() + container.getHeight() / 2f)) / (float) Game.HEIGHT * 2f - 1f;

    float textCenterX = text.getWidth() / (float) Game.WIDTH;
    float textCenterY = -text.getHeight() / (float) Game.HEIGHT;

    float shiftX = containerCenterX - textCenterX;
    float shiftY = containerCenterY - textCenterY;

    return new Matrix4f().translate(shiftX, shiftY, 0);
  }

  private Vao createVao(GuiElementMesh mesh) {
    int name = GL30.glGenVertexArrays();
    Vao vao = new Vao(name, mesh.size());
    GL30.glBindVertexArray(vao.name);

    vao.vertexBuffer = GL15.glGenBuffers();
    vao.indexBuffer = GL15.glGenBuffers();

    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vao.vertexBuffer);
    GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vao.indexBuffer);

    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, mesh.getVertices(), GL15.GL_STATIC_DRAW);
    GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, mesh.getIndices(), GL15.GL_STATIC_DRAW);

    GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 4 * 4, 0);
    GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 4 * 4, 2 * 4);
    GL20.glEnableVertexAttribArray(0);
    GL20.glEnableVertexAttribArray(1);

    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
    GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
    GL30.glBindVertexArray(0);

    return vao;
  }

  public void destory() {
    renderTargets.forEach(target -> {
      GL15.glDeleteBuffers(new int[] {target.vao.indexBuffer, target.vao.vertexBuffer});
      GL30.glDeleteVertexArrays(target.vao.name);
    });
    renderTargets.clear();
  }

  public void setTextColor(float r, float g, float b) {
    this.textColor.set(r, g, b);
  }
}
