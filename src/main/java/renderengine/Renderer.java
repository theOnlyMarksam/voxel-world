package renderengine;

import entity.CelestialBody;
import entity.PerspectiveCamera;
import files.textures.TextureMap;
import model.coordinate.ChunkCoordinate;
import model.coordinate.MeshCoordinate;
import model.mesh.Mesh;
import model.mesh.constants.MeshSeq;
import org.joml.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL42;
import shader.Shader;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Renderer {
  private Shader shader;
  private TextureMap textureMap;
  private CelestialBody sun;
  private int textureHandle;
  private PerspectiveCamera camera;

  private ShadowMapRenderer shadowMapRenderer;
  private CelestialBodyRenderer celestialBodyRenderer;

  private WaterRenderer waterRenderer;
  private WaterFrameBuffers waterFrameBuffers;

  private Map<ChunkCoordinate, List<Vao>> vertexArrayObjects = new HashMap<>();
  private Vector4f clipPlane = new Vector4f(0, 0, 0, 0);
  private static final float waterHeight = 25.999f;
  private GuiRenderer guiRenderer = new GuiRenderer();

  public Renderer(PerspectiveCamera camera, CelestialBody sun, CelestialBody moon, TextureMap textureMap) {
    this.camera = camera;
    this.sun = sun;
    this.textureMap = textureMap;
    this.shader = new Shader("default");

    this.shadowMapRenderer = new ShadowMapRenderer(camera, sun);
    this.celestialBodyRenderer = new CelestialBodyRenderer(camera, sun, moon);

    this.waterFrameBuffers = new WaterFrameBuffers();
    this.waterRenderer = new WaterRenderer(camera, sun, textureMap, waterFrameBuffers);
  }

  public void render() {
    GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
    int shadowTexture = shadowMapRenderer.render(vertexArrayObjects);

    if (camera.getPosition().y > waterHeight) {
      float initialHeight = camera.getPosition().y;
      float distance = camera.getPosition().y - waterHeight;
      camera.getPosition().y -= distance * 2;
      camera.invertPitch();
      camera.update();

      waterFrameBuffers.bindReflectionFrameBuffer();
      clipPlane.set(0, 1, 0, -waterHeight);
      renderLandMeshes(shadowTexture, clipPlane);

      camera.getPosition().y = initialHeight;
      camera.invertPitch();
      camera.update();

      waterFrameBuffers.bindRefractionFrameBuffer();
      clipPlane.set(0, -1, 0, waterHeight);
      renderLandMeshes(shadowTexture, clipPlane);
    }

    waterFrameBuffers.bindDefaultFrameBuffer();
    clipPlane.set(0, 0, 0, 0);
    renderLandMeshes(shadowTexture, clipPlane);

    celestialBodyRenderer.render();
    waterRenderer.render(vertexArrayObjects);
    guiRenderer.render();
  }

  private void renderLandMeshes(int shadowTexture, Vector4f clipPlane) {
    GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

    GL11.glEnable(GL11.GL_BLEND);
    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
    GL20.glUseProgram(shader.getProgram());

    shader.uniform1i("shadowTexture", 0);
    GL13.glActiveTexture(GL13.GL_TEXTURE0);
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, shadowTexture);

    textureHandle = sendTexture(textureMap, textureHandle);

    shader.uniformMatrix4fv("projection", camera.getProjectionMatrix());
    shader.uniformMatrix4fv("view", camera.getViewMatrix());
    shader.uniformMatrix4fv("lightSpaceMatrix", shadowMapRenderer.getLightSpaceMatrix());
    shader.uniform3f("lightPos", sun.getPosition());
    shader.uniform4f("clipPlane", clipPlane);

    vertexArrayObjects.entrySet()
        .stream()
        .filter(entry -> testChunkInViewport(entry.getKey(), camera))
        .map(entry -> entry.getValue().get(MeshSeq.LAND.getSeqNr()))
        .filter(Objects::nonNull)
        .sequential()
        .forEachOrdered(this::bindVaoAndRenderTriangles);

    GL20.glUseProgram(0);
    GL11.glDisable(GL11.GL_BLEND);
  }

  private int sendTexture(TextureMap textureMap, int textureHandle) {
    if (textureMap.isChanged()) {
      if (textureHandle != 0) {
        GL11.glDeleteTextures(textureHandle);
      }
      textureHandle = sendTextureDataToShader();
    }

    shader.uniform1i("tex", 1);
    GL13.glActiveTexture(GL13.GL_TEXTURE1);
    GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, textureHandle);

    return textureHandle;
  }

  private boolean testChunkInViewport(ChunkCoordinate coordinate, PerspectiveCamera camera) {
    return true;
  }

  private void bindVaoAndRenderTriangles(Vao vao) {
    GL30.glBindVertexArray(vao.vao);
    GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vao.size);
  }

  public void addVertexArrays(Collection<Mesh> meshes) {
    meshes.stream()
        .filter(mesh -> mesh.getSize() > 0)
        .forEach(mesh -> {
          MeshCoordinate crd = mesh.getPosition();
          List<Vao> existingVaos = vertexArrayObjects.get(crd.getChunkCoordinate());
          if (existingVaos == null) {
            Vao vao = createVertexArrayFromMesh(mesh);
            List<Vao> vaoList = Arrays
                                    .stream(MeshSeq.values())
                                    .map(seq -> (Vao) null)
                                    .collect(Collectors.toList());
            vaoList.set(crd.getSequenceNr().getSeqNr(), vao);
            vertexArrayObjects.put(crd.getChunkCoordinate(), vaoList);
          } else {
            Vao vaoInList = existingVaos.get(crd.getSequenceNr().getSeqNr());
            if (vaoInList != null) {
              GL15.glDeleteBuffers(getPrimitiveIntArrayFromCollection(vaoInList.vbo, Function.identity()));
              GL30.glDeleteVertexArrays(vaoInList.vao);
            }

            Vao vao = createVertexArrayFromMesh(mesh);
            existingVaos.set(crd.getSequenceNr().getSeqNr(), vao);
          }
        });
  }

  private Vao createVertexArrayFromMesh(Mesh mesh) {
    int vao = GL30.glGenVertexArrays();
    GL30.glBindVertexArray(vao);
    int vbo = GL15.glGenBuffers();
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, mesh.getVertexList().getDenseVertexArray(), GL15.GL_STATIC_DRAW);

    GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 9 * 4, 0);
    GL20.glEnableVertexAttribArray(0);

    GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 9 * 4, 3 * 4);
    GL20.glEnableVertexAttribArray(1);

    GL20.glVertexAttribPointer(2, 3, GL11.GL_FLOAT, false, 9 * 4, 6 * 4);
    GL20.glEnableVertexAttribArray(2);

    GL30.glBindVertexArray(0);

    return new Vao(vao, Collections.singletonList(vbo), mesh.getSize());
  }

  public void deleteVertexArrays(Collection<MeshCoordinate> removedMeshes) {
    if (removedMeshes == null || removedMeshes.isEmpty()) return;

    Set<ChunkCoordinate> coordinateSet = getRemovedChunkCoordinates(removedMeshes);
    List<Vao> deleteVaos = getVaoListToRemove(coordinateSet, vertexArrayObjects);

    if (!deleteVaos.isEmpty()) {
      coordinateSet.forEach(crd -> vertexArrayObjects.remove(crd));

      List<Integer> deleteVbos = getVboListToRemove(deleteVaos);

      int[] vaoArr = getPrimitiveIntArrayFromCollection(deleteVaos, obj -> obj.vao);
      int[] vboArr = getPrimitiveIntArrayFromCollection(deleteVbos, Function.identity());

      GL15.glDeleteBuffers(vboArr);
      GL30.glDeleteVertexArrays(vaoArr);
    }
  }

  private Set<ChunkCoordinate> getRemovedChunkCoordinates(Collection<MeshCoordinate> meshCoordinates) {
    return meshCoordinates
               .stream()
               .map(MeshCoordinate::getChunkCoordinate)
               .collect(Collectors.toSet());
  }

  private List<Vao> getVaoListToRemove(Set<ChunkCoordinate> coordinateSet,
                                       Map<ChunkCoordinate, List<Renderer.Vao>> vertexArrayObjects) {
    return vertexArrayObjects
               .entrySet()
               .stream()
               .filter(entry -> coordinateSet.contains(entry.getKey()))
               .flatMap(entry -> entry.getValue().stream())
               .filter(Objects::nonNull)
               .collect(Collectors.toList());
  }

  private <T> int[] getPrimitiveIntArrayFromCollection(List<T> list, Function<T, Integer> mappingFunction) {
    int[] vaoDeletePrimitive = new int[list.size()];
    IntStream.range(0, list.size()).forEach(i -> vaoDeletePrimitive[i] = mappingFunction.apply(list.get(i)));
    return vaoDeletePrimitive;
  }

  private List<Integer> getVboListToRemove(List<Vao> deleteVaos) {
    return deleteVaos
               .stream()
               .flatMap(vao -> vao.vbo.stream())
               .collect(Collectors.toList());
  }

  private int sendTextureDataToShader() {
    int mipLevelCount = 1; // TODO mip level should be asked from texture map
    int layerCount = textureMap.getLayerCount();

    int textureHandle = GL11.glGenTextures();
    GL13.glActiveTexture(GL13.GL_TEXTURE1);
    GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, textureHandle);

    GL42.glTexStorage3D(GL30.GL_TEXTURE_2D_ARRAY, mipLevelCount, GL11.GL_RGB8, 16, 16, layerCount);
    GL12.glTexSubImage3D(
        GL30.GL_TEXTURE_2D_ARRAY
        , 0
        , 0
        , 0
        , 0
        , 16
        , 16
        , layerCount
        , GL11.GL_RGBA
        , GL11.GL_UNSIGNED_BYTE
        , textureMap.getBytes()
    );

    // Always set reasonable texture parameters
    GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
    GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
    GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
    GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);

    return textureHandle;
  }

  public void setWaveLocation(float waveLocation) {
    waterRenderer.setMoveFactor(waveLocation);
  }

  public void shutDown() {
    vertexArrayObjects.values()
        .stream()
        .flatMap(Collection::stream)
        .filter(Objects::nonNull)
        .forEach(vao -> {
          vao.vbo.forEach(vbo -> GL15.glDeleteBuffers(vbo.intValue()));
          GL30.glDeleteVertexArrays(vao.vao);
        });
    vertexArrayObjects.clear();
  }

  class Vao {
    int vao;
    List<Integer> vbo;
    int size;

    Vao(int vao, List<Integer> vbo, int size) {
      this.vao = vao;
      this.vbo = vbo;
      this.size = size;
    }
  }
}
