package renderengine;

import entity.CelestialBody;
import entity.PerspectiveCamera;
import files.textures.Channels;
import files.textures.Image;
import files.textures.TextureMap;
import files.textures.TextureUtil;
import model.coordinate.ChunkCoordinate;
import model.mesh.constants.MeshSeq;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import shader.Shader;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class WaterRenderer {
  private Shader shader;
  private PerspectiveCamera camera;
  private CelestialBody sun;
  private TextureMap textureMap;
  private WaterFrameBuffers waterFrameBuffers;
  private int dudvTexture;
  private int normalMap;

  private float moveFactor = 0f;
  private Vector3f lightColor = new Vector3f(1.0f, 1.0f, 0.9f);

  public WaterRenderer(PerspectiveCamera camera, CelestialBody sun, TextureMap textureMap, WaterFrameBuffers waterFrameBuffers) {
    this.camera = camera;
    this.sun = sun;
    this.textureMap = textureMap;
    this.waterFrameBuffers = waterFrameBuffers;

    this.shader = new Shader("water");
    this.dudvTexture = loadTexture("map/dudv.png");
    this.normalMap = loadTexture("map/normal.png");
  }

  private int loadTexture(String textureName) {
    Image image = TextureUtil.loadImage(textureName, Channels.RGB);

    int texture = GL11.glGenTextures();
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

    GL11.glTexImage2D(
        GL11.GL_TEXTURE_2D
        , 0
        , GL11.GL_RGB
        , image.getWidth()
        , image.getHeight()
        , 0
        , GL11.GL_RGB
        , GL11.GL_UNSIGNED_BYTE
        , image.getImage()
    );

    return texture;
  }

  public void render(Map<ChunkCoordinate, List<Renderer.Vao>> vertexArrayObjects) {
    GL11.glEnable(GL11.GL_BLEND);
    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

    GL20.glUseProgram(shader.getProgram());
    shader.uniformMatrix4fv("projection", camera.getProjectionMatrix());
    shader.uniformMatrix4fv("view", camera.getViewMatrix());
    shader.uniform1f("moveFactor", moveFactor);
    shader.uniform3f("cameraPosition", camera.getPosition());
    shader.uniform3f("lightPosition", sun.getPosition());
    shader.uniform3f("lightColor", lightColor);

    shader.uniform1i("reflectionTexture", 0);
    GL13.glActiveTexture(GL13.GL_TEXTURE0);
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, waterFrameBuffers.getReflectionTexture());

    shader.uniform1i("refractionTexture", 1);
    GL13.glActiveTexture(GL13.GL_TEXTURE1);
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, waterFrameBuffers.getRefractionTexture());

    shader.uniform1i("dudvMap", 2);
    GL13.glActiveTexture(GL13.GL_TEXTURE2);
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, dudvTexture);

    shader.uniform1i("normalMap", 3);
    GL13.glActiveTexture(GL13.GL_TEXTURE3);
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, normalMap);

    shader.uniform1i("depthMap", 4);
    GL13.glActiveTexture(GL13.GL_TEXTURE4);
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, waterFrameBuffers.getRefractionDepthTexture());

    vertexArrayObjects.entrySet()
        .stream()
        .filter(entry -> testChunkInViewport(entry.getKey(), camera))
        .map(entry -> entry.getValue().get(MeshSeq.WATER.getSeqNr()))
        .filter(Objects::nonNull)
        .sequential()
        .forEachOrdered(this::bindVaoAndRenderTriangles);

    GL20.glUseProgram(0);
    GL11.glDisable(GL11.GL_BLEND);
  }

  private boolean testChunkInViewport(ChunkCoordinate coordinate, PerspectiveCamera camera) {
    return true;
  }

  private void bindVaoAndRenderTriangles(Renderer.Vao vao) {
    GL30.glBindVertexArray(vao.vao);
    GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vao.size);
  }

  public void setMoveFactor(float moveFactor) {
    this.moveFactor = moveFactor;
  }
}
