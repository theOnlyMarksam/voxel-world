package renderengine;

import org.joml.Matrix4f;

class GuiRenderTarget {
  Matrix4f transform;
  int texture;
  Vao vao;

  GuiRenderTarget(Matrix4f transform, int texture, Vao vao) {
    this.transform = transform;
    this.texture = texture;
    this.vao = vao;
  }

  int getVao() {
    return vao.name;
  }

  int getSize() {
    return vao.size;
  }

  public int indexBuffer() {
    return vao.indexBuffer;
  }
}
