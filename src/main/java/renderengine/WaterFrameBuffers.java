package renderengine;

import main.Game;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL32;

import java.nio.ByteBuffer;

public class WaterFrameBuffers {
  private static final int REFLECTION_WIDTH = 320;
  private static final int REFLECTION_HEIGHT = 180;

  private static final int REFRACTION_WIDTH = Game.WIDTH;
  private static final int REFRACTION_HEIGHT = Game.HEIGHT;

  private int reflectionFrameBuffer;
  private int reflectionTexture;
  private int reflectionDepthBuffer;

  private int refractionFrameBuffer;
  private int refractionTexture;
  private int refractionDepthTexture;

  public WaterFrameBuffers() {
    initializeReflectionFrameBuffer();
    initializeRefractionFrameBuffer();
    bindDefaultFrameBuffer();
  }

  private void initializeReflectionFrameBuffer() {
    reflectionFrameBuffer = createFrameBuffer();
    reflectionTexture = createTextureAttachment(REFLECTION_WIDTH, REFLECTION_HEIGHT);
    reflectionDepthBuffer = createDepthBufferAttachment(REFLECTION_WIDTH, REFLECTION_HEIGHT);
  }

  private void initializeRefractionFrameBuffer() {
    refractionFrameBuffer = createFrameBuffer();
    refractionTexture = createTextureAttachment(REFRACTION_WIDTH, REFRACTION_HEIGHT);
    refractionDepthTexture = createTexture(
        REFRACTION_WIDTH,
        REFRACTION_HEIGHT,
        GL14.GL_DEPTH_COMPONENT32,
        GL11.GL_DEPTH_COMPONENT,
        GL30.GL_DEPTH_ATTACHMENT
    );
  }

  private int createFrameBuffer() {
    int frameBuffer = GL30.glGenFramebuffers();
    GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, frameBuffer);
    GL11.glDrawBuffer(GL30.GL_COLOR_ATTACHMENT0);
    return frameBuffer;
  }

  private int createTextureAttachment(int width, int height) {
    return createTexture(width, height, GL11.GL_RGB, GL11.GL_RGB, GL30.GL_COLOR_ATTACHMENT0);
  }

  private int createDepthBufferAttachment(int width, int height) {
    int depthBuffer = GL30.glGenRenderbuffers();
    GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, depthBuffer);
    GL30.glRenderbufferStorage(GL30.GL_RENDERBUFFER, GL11.GL_DEPTH_COMPONENT, width,
        height);
    GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_ATTACHMENT,
        GL30.GL_RENDERBUFFER, depthBuffer);
    return depthBuffer;
  }

  private int createTexture(int width, int height, int internalFormat, int format, int attachment) {
    int texture = GL11.glGenTextures();
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);
    GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, internalFormat, width, height,
        0, format, GL11.GL_FLOAT, (ByteBuffer) null);

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);

    GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, attachment, texture, 0);
    return texture;
  }


  public void bindReflectionFrameBuffer() {
    bindFrameBuffer(reflectionFrameBuffer, REFLECTION_WIDTH, REFLECTION_HEIGHT);
  }

  public void bindRefractionFrameBuffer() {
    bindFrameBuffer(refractionFrameBuffer, REFRACTION_WIDTH, REFRACTION_HEIGHT);
  }

  private void bindFrameBuffer(int frameBuffer, int width, int height){
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
    GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, frameBuffer);
    GL11.glViewport(0, 0, width, height);
  }

  public void bindDefaultFrameBuffer() {
    GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
    GL11.glViewport(0, 0, Game.WIDTH, Game.HEIGHT);
  }

  public int getReflectionTexture() {
    return reflectionTexture;
  }

  public int getRefractionTexture() {
    return refractionTexture;
  }

  public int getRefractionDepthTexture() {
    return refractionDepthTexture;
  }
}
