package renderengine;

import entity.CelestialBody;
import entity.PerspectiveCamera;
import main.Game;
import model.coordinate.ChunkCoordinate;
import model.mesh.constants.MeshSeq;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.*;
import shader.Shader;
import shader.ShaderUniform;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ShadowMapRenderer {
  private static final int SIZE = 4096;
  private int shadowMapFBO;
  private int shadowTexture;
  private CelestialBody sun;
  private Shader shader;
  private Matrix4f lightSpaceMatrix;
  private PerspectiveCamera camera;

  public ShadowMapRenderer(PerspectiveCamera camera, CelestialBody sun) {
    createFrameBuffer();
    this.shader = new Shader("shadow_mapping");
    this.camera = camera;
    this.sun = sun;
  }

  private void createFrameBuffer() {
    this.shadowMapFBO = GL30.glGenFramebuffers();
    this.shadowTexture = GL11.glGenTextures();

    GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.shadowTexture);
    GL11.glTexImage2D(
        GL11.GL_TEXTURE_2D,
        0,
        GL14.GL_DEPTH_COMPONENT32,
        SIZE,
        SIZE,
        0,
        GL11.GL_DEPTH_COMPONENT,
        GL11.GL_FLOAT,
        (ByteBuffer) null
    );

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);

    GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, this.shadowMapFBO);
    GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_ATTACHMENT, GL11.GL_TEXTURE_2D, shadowTexture, 0);
    GL11.glDrawBuffer(GL11.GL_NONE);
    GL11.glReadBuffer(GL11.GL_NONE);
    GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
  }

  int render(Map<ChunkCoordinate, List<Renderer.Vao>> vertexArrayObjects) {
    GL20.glUseProgram(shader.getProgram());
    lightSpaceMatrix = createMvpMatrix();
    shader.uniformMatrix4fv(ShaderUniform.MVP_MATRIX.getName(), lightSpaceMatrix);

    GL11.glViewport(0, 0, SIZE, SIZE);
    GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, this.shadowMapFBO);
    GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
    GL11.glCullFace(GL11.GL_FRONT);
    GL11.glEnable(GL11.GL_POLYGON_OFFSET_FILL);
    GL11.glPolygonOffset(2.0f, 4.0f);

    vertexArrayObjects.values()
        .stream()
        .map(vaos -> vaos.get(MeshSeq.LAND.getSeqNr()))
        .filter(Objects::nonNull)
        .sequential()
        .forEachOrdered(this::bindVaoAndRenderTriangles);

    GL11.glDisable(GL11.GL_POLYGON_OFFSET_FILL);
    GL11.glCullFace(GL11.GL_BACK);
    GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
    GL11.glViewport(0, 0, Game.WIDTH, Game.HEIGHT);
    GL20.glUseProgram(0);

    return shadowTexture;
  }

  private void bindVaoAndRenderTriangles(Renderer.Vao vao) {
    GL30.glBindVertexArray(vao.vao);
    GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vao.size);
  }

  Matrix4f getLightSpaceMatrix() {
    return lightSpaceMatrix;
  }

  private Matrix4f createMvpMatrix() {
    float distance = 1e5f;
    Vector3f eyePos = new Vector3f(
        sun.getPosition().x,
        sun.getPosition().y - camera.getPosition().y,
        sun.getPosition().z
    );
    eyePos.normalize(distance);
    Matrix4f lightView = new Matrix4f().lookAt(
        eyePos,
        new Vector3f(camera.getPosition().x, 0.0f, camera.getPosition().z),
        new Vector3f(0, 1, 0)
    );
    Matrix4f lightProjection = new Matrix4f().ortho(
        sun.getPosition().x + 100
        , sun.getPosition().x - 100
        , -100
        , 100
        , distance - 200
        , distance
    );

    Matrix4f result = new Matrix4f();
    return lightProjection.mul(lightView, result);
  }
}
