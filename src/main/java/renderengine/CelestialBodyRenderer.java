package renderengine;

import entity.CelestialBody;
import entity.PerspectiveCamera;
import shader.Shader;
import shader.ShaderAttrib;
import shader.ShaderUniform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

public class CelestialBodyRenderer {
  private static final float[] vertices = {
      // TR1
      -60f, 60f, -60f, 1f, 1f,
      60f, 60f, 60f, 0f, 0f,
      60f, 60f, -60f, 0f, 1f,
      // TR2
      -60f, 60f, -60f, 1f, 1f,
      -60f, 60f, 60f, 1f, 0f,
      60f, 60f, 60f, 0f, 0f,
  };

  private Shader shader;
  private List<CelestialBody> bodies = new ArrayList<>();

  private int vao;
  private PerspectiveCamera camera;

  public CelestialBodyRenderer(PerspectiveCamera camera, CelestialBody sun, CelestialBody moon) {
    this.camera = camera;
    this.shader = new Shader("celestial");

    bodies.addAll(Arrays.asList(sun, moon));

    sendTextureDataToShader(sun);
    sendTextureDataToShader(moon);

    generateVao();
  }

  private void generateVao() {
    vao = glGenVertexArrays();
    glBindVertexArray(vao);

    int vbo = glGenBuffers();
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);

    int vertexPos = glGetAttribLocation(shader.getProgram(), ShaderAttrib.POSITION.getName());
    int uvPos = glGetAttribLocation(shader.getProgram(), ShaderAttrib.UV.getName());

    glVertexAttribPointer(vertexPos, 3, GL_FLOAT, false, 5 * 4, 0);
    glEnableVertexAttribArray(vertexPos);

    glVertexAttribPointer(uvPos, 2, GL_FLOAT, false, 5 * 4, 3 * 4);
    glEnableVertexAttribArray(uvPos);

    glBindVertexArray(0);
  }

  public void render() {
    glUseProgram(shader.getProgram());

    shader.uniformMatrix4fv(ShaderUniform.PROJECTION.getName(), camera.getProjectionMatrix());
    shader.uniformMatrix4fv(ShaderUniform.VIEW.getName(), camera.getViewMatrix());

    for (CelestialBody body : bodies) {
      shader.uniformMatrix4fv(ShaderUniform.MODEL.getName(), body.getModelMatrix());

      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, body.getTextureId());

      glBindVertexArray(vao);
      glDrawArrays(GL_TRIANGLES, 0, vertices.length / 5);
    }

    glUseProgram(0);
  }

  private void sendTextureDataToShader(CelestialBody body) {
    int texture = glGenTextures();
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(
        GL_TEXTURE_2D
        , 0
        , GL_RGB
        , body.getTexture().getWidth()
        , body.getTexture().getHeight()
        , 0
        , GL_RGB
        , GL_UNSIGNED_BYTE
        , body.getTexture().getImage()
    );

    body.setTextureId(texture);
  }
}
