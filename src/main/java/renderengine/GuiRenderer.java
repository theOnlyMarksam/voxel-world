package renderengine;

import files.textures.Channels;
import files.textures.Image;
import main.Game;
import mechanics.element.GuiElement;
import mechanics.element.GuiElementMesh;
import mechanics.element.Rectangle;
import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import shader.Shader;

import java.util.ArrayList;
import java.util.List;

public class GuiRenderer {
  private static final float[] positions = {
      -1,  1,   0, 0,
      -1, -1,   0, 1,
       1,  1,   1, 0,
       1, -1,   1, 1
  };

  private static final int[] indices = {
      0, 1, 2,
      2, 1, 3
  };

  private static final float[] defaultBgTextureData = {
      0.0f, 0.0f, 0.0f
  };

  private static final float[] testTexture = {
      1.0f, 1.0f, 0.0f,  1.0f, 0.0f, 0.0f,
      0.0f, 1.0f, 0.0f,  0.0f, 0.0f, 1.0f,
  };
  private int defaultButtonTexture;
  private int defaultBgTexture;

  private boolean clearScreen = false;
  private boolean blending = false;
  private Shader shader;
  private int vao;
  private List<GuiRenderTarget> renderTargets = new ArrayList<>();

  public GuiRenderer() {
    this.shader = new Shader("gui");
    this.vao = initVao();
    this.defaultButtonTexture = initTexture(testTexture, 2, 2);
    this.defaultBgTexture = initTexture(defaultBgTextureData, 1, 1);
  }

  private int initVao() {
    int vao = GL30.glGenVertexArrays();
    GL30.glBindVertexArray(vao);

    int vbo = GL15.glGenBuffers();
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, positions, GL15.GL_STATIC_DRAW);

    GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 4 * 4, 0);
    GL20.glEnableVertexAttribArray(0);

    GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 4 * 4, 2 * 4);
    GL20.glEnableVertexAttribArray(1);

    GL30.glBindVertexArray(0);

    return vao;
  }

  private int initTexture(float[] textureData, int width, int height) {
    int texture = GL11.glGenTextures();
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

    GL11.glTexImage2D(
        GL11.GL_TEXTURE_2D
        , 0
        , GL11.GL_RGB
        , width
        , height
        , 0
        , GL11.GL_RGB
        , GL11.GL_FLOAT
        , textureData
    );

    return texture;
  }

  public void render() {
    if (clearScreen) {
      GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
    }

    if (blending) {
      GL11.glEnable(GL11.GL_BLEND);
      GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
    }

    GL11.glDisable(GL11.GL_DEPTH_TEST);
    GL20.glUseProgram(shader.getProgram());

    GL13.glActiveTexture(GL13.GL_TEXTURE0);
    shader.uniform1i("guiTexture", 0);

    for (GuiRenderTarget renderTarget: renderTargets) {
      GL30.glBindVertexArray(renderTarget.getVao());
      GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, renderTarget.indexBuffer());
      GL11.glBindTexture(GL11.GL_TEXTURE_2D, renderTarget.texture);
      shader.uniformMatrix4fv("transformationMatrix", renderTarget.transform);
      GL11.glDrawElements(GL11.GL_TRIANGLES, renderTarget.getSize(), GL11.GL_UNSIGNED_INT, 0);
    }

    GL20.glUseProgram(0);
    GL11.glEnable(GL11.GL_DEPTH_TEST);

    if (blending) {
      GL11.glDisable(GL11.GL_BLEND);
    }
  }

  public void bindGuiElement(GuiElement guiElement) {
    int tex = guiElement.getTexture() == null ? defaultButtonTexture : createTexture(guiElement.getTexture());
    Matrix4f transform = createTranslate(guiElement.getRectangle());
    Vao vao = createVao(guiElement.getMesh());

    renderTargets.add(new GuiRenderTarget(transform, tex, vao));
  }

  private int createTexture(Image image) {
    int format = image.getChannels() == Channels.RGB ? GL11.GL_RGB : GL11.GL_RGBA;
    int texture = GL11.glGenTextures();
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

    GL11.glTexImage2D(
      GL11.GL_TEXTURE_2D,
      0,
      format,
      image.getWidth(),
      image.getHeight(),
      0,
      format,
      GL11.GL_UNSIGNED_BYTE,
      image.getImage()
    );

    return texture;
  }

  private Matrix4f createTranslate(Rectangle rectangle) {
    float centerX = rectangle.getX() + rectangle.getWidth() / 2f;
    float centerY = rectangle.getY() + rectangle.getHeight() / 2f;

    float screenSpaceX = centerX / (float) Game.WIDTH * 2f - 1f;
    float screenSpaceY = (Game.HEIGHT - centerY) / (float) Game.HEIGHT * 2f - 1f;

    return new Matrix4f().translate(screenSpaceX, screenSpaceY, 0.0f);
  }

  private Vao createVao(GuiElementMesh mesh) {
    int name = GL30.glGenVertexArrays();
    Vao vao = new Vao(name, mesh.size());
    GL30.glBindVertexArray(vao.name);

    vao.vertexBuffer = GL15.glGenBuffers();
    vao.indexBuffer = GL15.glGenBuffers();

    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vao.vertexBuffer);
    GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vao.indexBuffer);

    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, mesh.getVertices(), GL15.GL_STATIC_DRAW);
    GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, mesh.getIndices(), GL15.GL_STATIC_DRAW);

    GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 4 * 4, 0);
    GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 4 * 4, 2 * 4);
    GL20.glEnableVertexAttribArray(0);
    GL20.glEnableVertexAttribArray(1);

    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
    GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
    GL30.glBindVertexArray(0);

    return vao;
  }

  public void setClearScreen(boolean clearScreen) {
    this.clearScreen = clearScreen;
  }

  public void destroy() {
    renderTargets.forEach(renderTarget -> {
      GL15.glDeleteBuffers(new int[] {renderTarget.vao.indexBuffer, renderTarget.vao.vertexBuffer});
      GL30.glDeleteVertexArrays(renderTarget.vao.name);
    });
  }

  public void setBlending(boolean blending) {
    this.blending = blending;
  }
}
