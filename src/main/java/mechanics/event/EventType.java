package mechanics.event;

public enum EventType {
  KEY,
  MOUSE,
  BUTTON,
  DEFAULT
}
