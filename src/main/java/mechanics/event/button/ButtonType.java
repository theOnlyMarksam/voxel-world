package mechanics.event.button;

public enum ButtonType {
  START,
  OPTIONS,
  RESUME,
  EXIT
}
