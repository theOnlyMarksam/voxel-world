package mechanics.event.button;

import mechanics.event.Event;
import mechanics.event.EventType;
import mechanics.observer.Observable;

public class ButtonEvent implements Event {
  private ButtonType buttonType;
  private Observable emitter;

  private ButtonEvent(ButtonType buttonType, Observable emitter) {
    this.buttonType = buttonType;
    this.emitter = emitter;
  }

  public ButtonType getButtonType() {
    return buttonType;
  }

  @Override
  public EventType getType() {
    return EventType.BUTTON;
  }

  @Override
  public Observable getEmitter() {
    return emitter;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ButtonEvent that = (ButtonEvent) o;

    return buttonType == that.buttonType;

  }

  @Override
  public String toString() {
    return "ButtonEvent{" +
               "buttonType=" + buttonType.name() +
               ", emitter=" + emitter +
               '}';
  }

  @Override
  public int hashCode() {
    return buttonType.hashCode();
  }

  public static ButtonEvent of(ButtonType buttonType, Observable emitter) {
    return new ButtonEvent(buttonType, emitter);
  }
}
