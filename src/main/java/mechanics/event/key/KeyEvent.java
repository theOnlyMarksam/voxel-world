package mechanics.event.key;

import mechanics.event.Event;
import mechanics.event.EventType;
import mechanics.observer.Observable;

public class KeyEvent implements Event {
  private int button;
  private int action;
  private Observable emitter;

  private KeyEvent(int button, int action, Observable emitter) {
    this.button = button;
    this.action = action;
    this.emitter = emitter;
  }

  public int getButton() {
    return button;
  }

  public int getAction() {
    return action;
  }

  @Override
  public EventType getType() {
    return EventType.KEY;
  }

  @Override
  public Observable getEmitter() {
    return emitter;
  }

  public static KeyEvent of(int button, int action, Observable emitter) {
    return new KeyEvent(button, action, emitter);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    KeyEvent keyEvent = (KeyEvent) o;

    if (button != keyEvent.button) return false;
    return action == keyEvent.action;

  }

  @Override
  public int hashCode() {
    int result = button;
    result = 31 * result + action;
    return result;
  }
}
