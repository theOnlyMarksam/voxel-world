package mechanics.event;

import mechanics.observer.Observable;

public interface Event {
  EventType getType();

  Observable getEmitter();
}
