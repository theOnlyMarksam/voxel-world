package mechanics.event.mouse;

import mechanics.observer.Observable;

public class MouseMoveEvent extends MouseEvent {
  private int x;
  private int y;

  private MouseMoveEvent(Observable emitter) {
    super(emitter);
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public static MouseMoveEvent of(double x, double y, Observable emitter) {
    MouseMoveEvent event = new MouseMoveEvent(emitter);
    event.x = (int) x;
    event.y = (int) y;

    return event;
  }
}
