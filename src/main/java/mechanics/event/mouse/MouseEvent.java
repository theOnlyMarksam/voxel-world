package mechanics.event.mouse;

import mechanics.event.Event;
import mechanics.event.EventType;
import mechanics.observer.Observable;

public abstract class MouseEvent implements Event {
  private Observable emitter;

  public MouseEvent(Observable emitter) {
    this.emitter = emitter;
  }

  @Override
  public Observable getEmitter() {
    return emitter;
  }

  @Override
  public EventType getType() {
    return EventType.MOUSE;
  }
}
