package mechanics.event.mouse;

import mechanics.observer.Observable;
import org.lwjgl.glfw.GLFW;

public class MouseClickEvent extends MouseEvent {
  private int button;
  private int action;
  private int x;
  private int y;

  private MouseClickEvent(Observable emitter) {
    super(emitter);
  }

  public int getButton() {
    return button;
  }

  public int getAction() {
    return action;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public boolean lmbPressed() {
    return button == GLFW.GLFW_MOUSE_BUTTON_LEFT && action == GLFW.GLFW_PRESS;
  }

  public boolean lmbReleased() {
    return button == GLFW.GLFW_MOUSE_BUTTON_LEFT && action == GLFW.GLFW_RELEASE;
  }

  public boolean rmbPressed() {
    return button == GLFW.GLFW_MOUSE_BUTTON_RIGHT && action == GLFW.GLFW_PRESS;
  }

  public boolean rmbReleased() {
    return button == GLFW.GLFW_MOUSE_BUTTON_RIGHT && action == GLFW.GLFW_RELEASE;
  }

  public static MouseClickEvent of(int button, int action, int x, int y, Observable emitter) {
    MouseClickEvent event = new MouseClickEvent(emitter);
    event.button = button;
    event.action = action;
    event.x = x;
    event.y = y;

    return event;
  }

  @Override
  public String toString() {
    return "MouseClickEvent{" +
               "button=" + button +
               ", action=" + action +
               ", x=" + x +
               ", y=" + y +
               ", emitter=" + getEmitter() +
               '}';
  }
}
