package mechanics.event.mouse;

import mechanics.observer.Observable;

public class MouseExitEvent extends MouseEvent {

  private MouseExitEvent(Observable emitter) {
    super(emitter);
  }


  public static MouseExitEvent of(Observable emitter) {
    return new MouseExitEvent(emitter);
  }
}
