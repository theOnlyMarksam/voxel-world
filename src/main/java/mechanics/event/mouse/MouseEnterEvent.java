package mechanics.event.mouse;

import mechanics.observer.Observable;

public class MouseEnterEvent extends MouseEvent {

  private MouseEnterEvent(Observable emitter) {
    super(emitter);
  }


  public static MouseEnterEvent of(Observable emitter) {
    return new MouseEnterEvent(emitter);
  }
}
