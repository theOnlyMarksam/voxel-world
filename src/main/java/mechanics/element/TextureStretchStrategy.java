package mechanics.element;

public enum TextureStretchStrategy {
  NONE,
  X,
  Y,
  BOTH
}
