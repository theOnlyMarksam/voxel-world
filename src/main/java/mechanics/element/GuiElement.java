package mechanics.element;

import files.textures.Image;

public interface GuiElement {

  Image getTexture();

  Rectangle getRectangle();

  GuiElementMesh getMesh();
}
