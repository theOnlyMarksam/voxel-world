package mechanics.element;

public class GuiElementMesh {
  private float[] vertices;
  private int[] indices;

  public GuiElementMesh() {

  }

  public GuiElementMesh(float[] vertices, int[] indices) {
    this.vertices = vertices;
    this.indices = indices;
  }

  public void setVertices(float[] vertices) {
    this.vertices = vertices;
  }

  public void setIndices(int[] indices) {
    this.indices = indices;
  }

  public float[] getVertices() {
    return vertices;
  }

  public int[] getIndices() {
    return indices;
  }

  public int size() {
    return indices.length;
  }
}
