package mechanics.element;

/**
 * All dimensions are in pixels. Point (0, 0) is in top left corner and point
 * ({@link main.Game#WIDTH}, {@link main.Game#HEIGHT}) is in lower right corner of the screen.
 */
public class Rectangle {
  private int x;
  private int y;
  private int width;
  private int height;

  public Rectangle(int x, int y, int width, int height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public boolean inside(int x, int y) {
    return this.x <= x &&
           this.y <= y &&
           (this.x + width) >= x &&
           (this.y + height) >= y;
  }
}
