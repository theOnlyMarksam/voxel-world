package mechanics.element;

import files.textures.Channels;
import files.textures.Image;
import main.Game;

import java.nio.ByteBuffer;

public class GuiMeshGenerator {

  public static GuiElementMesh generateMesh(Image elementTexture, TextureStretchStrategy strategy, int w, int h) {

    if (strategy == TextureStretchStrategy.BOTH) {
      return generateMeshWithBothAxesStretched(elementTexture, w, h);
    } else if (strategy == TextureStretchStrategy.NONE) {
      return generateNotStretchedMesh(w, h);
    }
    throw new IllegalArgumentException("Wrong texture stretch strategy");
  }

  private static GuiElementMesh generateMeshWithBothAxesStretched(Image elementTexture, int w, int h) {
    GuiElementMesh mesh = new GuiElementMesh();
    mesh.setIndices(getIndexBuffer());

    int posSize = 2;
    int uvSize = 2;
    int vertexSize = posSize * uvSize;
    float[] vertices = new float[16 * vertexSize];
    fillConstantVertices(vertices, vertexSize, w, h);

    fillDynamicVertices(vertices, vertexSize, elementTexture, w, h);

    mesh.setVertices(vertices);
    return mesh;
  }

  private static int[] getIndexBuffer() {
    return new int[]{
        0, 4, 5,
        0, 5, 1,
        1, 5, 6,
        1, 6, 2,
        2, 6, 7,
        2, 7, 3,
        4, 8, 9,
        4, 9, 5,
        5, 9, 10,
        5, 10, 6,
        6, 10, 11,
        6, 11, 7,
        8, 12, 13,
        8, 13, 9,
        9, 13, 14,
        9, 14, 10,
        10, 14, 15,
        10, 15, 11
    };
  }

  private static void fillConstantVertices(float[] vertices, int vSize, int w, int h) {
    float x = w / (float) Game.WIDTH;
    float y = h / (float) Game.HEIGHT;

    vertices[0] = -x;                   // x
    vertices[1] = y;                    // y
    vertices[2] = 0;                    // u
    vertices[3] = 0;                    // v

    vertices[3 * vSize] = x;            // x
    vertices[3 * vSize + 1] = y;        // y
    vertices[3 * vSize + 2] = 1;        // u
    vertices[3 * vSize + 3] = 0;        // v

    vertices[12 * vSize] = -x;          // x
    vertices[12 * vSize + 1] = -y;      // y
    vertices[12 * vSize + 2] = 0;       // u
    vertices[12 * vSize + 3] = 1;       // v

    vertices[15 * vSize] = x;           // x
    vertices[15 * vSize + 1] = -y;      // y
    vertices[15 * vSize + 2] = 1;       // u
    vertices[15 * vSize + 3] = 1;       // v
  }

  private static void fillDynamicVertices(float[] vertices, int vSize, Image elementTexture, int w, int h) {
    float stretchedRow = getStretchedRow(elementTexture);
    float stretchedColumn = getStretchedColumn(elementTexture);
    elementTexture.getImage().rewind();

    float actualRowCount = elementTexture.getHeight() - 1;
    float actualColCount = elementTexture.getWidth() - 1;

    float pixelWidth = 2f / (float) Game.WIDTH;
    float pixelHeight = 2f / (float) Game.HEIGHT;

    float x = w / (float) Game.WIDTH;
    float y = h / (float) Game.HEIGHT;

    float x1 = -x + pixelWidth * stretchedColumn;
    float x2 = x - pixelWidth * (actualColCount - 1 - stretchedColumn);
    float y1 = y - pixelHeight * stretchedRow;
    float y2 = -y + pixelHeight * (actualRowCount - 1 - stretchedRow);

    float u1 = stretchedColumn / actualColCount;
    float u2 = (actualColCount - 1 - stretchedColumn) / actualColCount;
    float v1 = stretchedRow / actualRowCount;
    float v2 = (actualRowCount - 1 - stretchedRow) / actualRowCount;

    fill(vertices, vSize, x, y, x1, x2, y1, y2, u1, u2, v1, v2);
  }

  private static float getStretchedRow(Image elementTexture) {
    int channelCount = elementTexture.getChannels() == Channels.RGB ? 3 : 4;

    byte[] bitmapRow = new byte[elementTexture.getWidth() * channelCount];
    ByteBuffer bitmap = elementTexture.getImage();
    bitmap.rewind();

    for (int row = 0; row < elementTexture.getHeight() - 1; row++) {
      bitmap.get(bitmapRow);

      if (firstPixelBlack(bitmapRow, channelCount)) {
        return row;
      }
    }

    return -1;
  }

  private static boolean firstPixelBlack(byte[] bitmapRow, int channelCount) {
    // do not compare alpha channel if it exists
    int c = Math.min(3, channelCount);

    for (int i = 0; i < c; i++) {
      if (bitmapRow[i] != 0) {
        return false;
      }
    }
    return true;
  }

  private static float getStretchedColumn(Image elementTexture) {
    int channelCount = elementTexture.getChannels() == Channels.RGB ? 3 : 4;

    byte[] lastBitmapRow = new byte[elementTexture.getWidth() * channelCount];
    ByteBuffer bitmap = elementTexture.getImage();
    bitmap.rewind();

    bitmap.position(elementTexture.getWidth() * channelCount * (elementTexture.getHeight() - 1));
    bitmap.get(lastBitmapRow);

    return blackPixelPosition(lastBitmapRow, channelCount);
  }

  private static float blackPixelPosition(byte[] lastBitmapRow, int channelCount) {
    // do not compare alpha channel if it exists
    int c = Math.min(3, channelCount);

    for (int pos = 1; pos < lastBitmapRow.length / channelCount; pos++) {
      int darkChannelCount = 0;

      for (int b = 0; b < c; b++) {
        if (lastBitmapRow[pos * channelCount + b] != 0) {
          break;
        }
        darkChannelCount++;
      }

      if (darkChannelCount == c) {
        return pos - 1;
      }
    }

    return -1;
  }

  private static void fill(float[] vertices, int vSize, float x, float y, float x1, float x2,
                           float y1, float y2, float u1, float u2, float v1, float v2) {
    vertices[vSize] = x1;
    vertices[vSize + 1] = y;
    vertices[vSize + 2] = u1;
    vertices[vSize + 3] = 0;

    vertices[2 * vSize] = x2;
    vertices[2 * vSize + 1] = y;
    vertices[2 * vSize + 2] = u2;
    vertices[2 * vSize + 3] = 0;

    vertices[4 * vSize] = -x;
    vertices[4 * vSize + 1] = y1;
    vertices[4 * vSize + 2] = 0;
    vertices[4 * vSize + 3] = v1;

    vertices[5 * vSize] = x1;
    vertices[5 * vSize + 1] = y1;
    vertices[5 * vSize + 2] = u1;
    vertices[5 * vSize + 3] = v1;

    vertices[6 * vSize] = x2;
    vertices[6 * vSize + 1] = y1;
    vertices[6 * vSize + 2] = u2;
    vertices[6 * vSize + 3] = v1;

    vertices[7 * vSize] = x;
    vertices[7 * vSize + 1] = y1;
    vertices[7 * vSize + 2] = 1;
    vertices[7 * vSize + 3] = v1;

    vertices[8 * vSize] = -x;
    vertices[8 * vSize + 1] = y2;
    vertices[8 * vSize + 2] = 0;
    vertices[8 * vSize + 3] = v2;

    vertices[9 * vSize] = x1;
    vertices[9 * vSize + 1] = y2;
    vertices[9 * vSize + 2] = u1;
    vertices[9 * vSize + 3] = v2;

    vertices[10 * vSize] = x2;
    vertices[10 * vSize + 1] = y2;
    vertices[10 * vSize + 2] = u2;
    vertices[10 * vSize + 3] = v2;

    vertices[11 * vSize] = x;
    vertices[11 * vSize + 1] = y2;
    vertices[11 * vSize + 2] = 1;
    vertices[11 * vSize + 3] = v2;

    vertices[13 * vSize] = x1;
    vertices[13 * vSize + 1] = -y;
    vertices[13 * vSize + 2] = u1;
    vertices[13 * vSize + 3] = 1;

    vertices[14 * vSize] = x2;
    vertices[14 * vSize + 1] = -y;
    vertices[14 * vSize + 2] = u2;
    vertices[14 * vSize + 3] = 1;
  }

  private static GuiElementMesh generateNotStretchedMesh(int w, int h) {
    GuiElementMesh mesh = new GuiElementMesh();
    mesh.setIndices(getSimpleIndices());
    mesh.setVertices(getSimpleVertices(w, h));

    return mesh;
  }

  private static int[] getSimpleIndices() {
    return new int[] {
        0, 2, 3,
        0, 3, 1
    };
  }

  private static float[] getSimpleVertices(int w, int h) {
    float x = w / (float) Game.WIDTH;
    float y = h / (float) Game.HEIGHT;

    return new float[] {
        -x, y, 0, 0,
        x, y, 1, 0,
        -x, -y, 0, 1,
        x, -y, 1, 1
    };
  }
}
