package mechanics.element;

import files.font.bitmap.Font;
import files.font.bitmap.FontChar;
import main.ApplicationContext;
import main.Game;

public class FontMeshGenerator {
  private static final int POS_SIZE = 2;
  // support only one page per font
  private static final int UV_SIZE = 2;
  private static final int VERTEX_SIZE = POS_SIZE + UV_SIZE;

  public static GuiElementMesh generateMesh(String text) {
    Font font = ApplicationContext.getFont();
    int bitmapWidth = font.getFontCommon().getScaleW();
    int bitmapHeight = font.getFontCommon().getScaleH();
    float[] vertices = new float[4 * VERTEX_SIZE * text.length()];
    int[] indices = new int[6 * text.length()];
    int quadCount = 0;
    float pixelWidth = 2f / (float) Game.WIDTH;
    float pixelHeight = 2f / (float) Game.HEIGHT;

    float xPos = 0;
    float yPos = 0;

    for (int i = 0; i < text.length(); i++) {
      FontChar fontChar = font.getChar(text.charAt(i));
      xPos = addChar(fontChar, vertices, indices, quadCount, pixelWidth, pixelHeight, xPos, yPos, bitmapWidth, bitmapHeight);
      quadCount++;
    }

    return new GuiElementMesh(vertices, indices);
  }

  private static float addChar(FontChar fontChar,
                               float[] vertices,
                               int[] indices,
                               int quadCount,
                               float pixelWidth,
                               float pixelHeight,
                               float xPos,
                               float yPos,
                               float bitmapWidth,
                               float bitmapHeight) {
    int vIndex = quadCount * 4 * VERTEX_SIZE;
    int iIndex = quadCount * 6;

    float modifiedX = xPos + fontChar.getxOffset();
    float modifiedY = yPos - fontChar.getyOffset();

    vertices[vIndex     ] = modifiedX * pixelWidth;     // x
    vertices[vIndex +  1] = modifiedY * pixelHeight;     // y
    vertices[vIndex +  2] = fontChar.getX() / bitmapWidth;     // u
    vertices[vIndex +  3] = fontChar.getY() / bitmapHeight;     // v

    vertices[vIndex +  4] = (modifiedX + fontChar.getWidth()) * pixelWidth;
    vertices[vIndex +  5] = modifiedY * pixelHeight;
    vertices[vIndex +  6] = (fontChar.getX() + fontChar.getWidth()) / bitmapWidth;
    vertices[vIndex +  7] = fontChar.getY() / bitmapHeight;

    vertices[vIndex +  8] = modifiedX * pixelWidth;
    vertices[vIndex +  9] = (modifiedY - fontChar.getHeight()) * pixelHeight;
    vertices[vIndex + 10] = fontChar.getX() / bitmapWidth;
    vertices[vIndex + 11] = (fontChar.getY() + fontChar.getHeight()) / bitmapHeight;

    vertices[vIndex + 12] = (modifiedX + fontChar.getWidth()) * pixelWidth;
    vertices[vIndex + 13] = (modifiedY - fontChar.getHeight()) * pixelHeight;
    vertices[vIndex + 14] = (fontChar.getX() + fontChar.getWidth()) / bitmapWidth;
    vertices[vIndex + 15] = (fontChar.getY() + fontChar.getHeight()) / bitmapHeight;

    indices[iIndex    ] = quadCount * 4;
    indices[iIndex + 1] = quadCount * 4 + 2;
    indices[iIndex + 2] = quadCount * 4 + 1;

    indices[iIndex + 3] = quadCount * 4 + 1;
    indices[iIndex + 4] = quadCount * 4 + 2;
    indices[iIndex + 5] = quadCount * 4 + 3;

    return xPos + fontChar.getxAdvance();
  }
}
