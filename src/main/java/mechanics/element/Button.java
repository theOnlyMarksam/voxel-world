package mechanics.element;

import files.textures.Channels;
import files.textures.Image;
import files.textures.TextureUtil;
import mechanics.event.button.ButtonEvent;
import mechanics.event.button.ButtonType;
import mechanics.event.Event;
import mechanics.event.EventType;
import mechanics.event.mouse.MouseClickEvent;
import mechanics.event.mouse.MouseEvent;
import mechanics.observer.Observable;
import mechanics.observer.Observer;
import model.gui.text.GuiText;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class Button implements Observer, Observable, GuiElement {
  private static final Logger LOG = LogManager.getFormatterLogger(Button.class);

  private final List<Observer> observers = new ArrayList<>();
  private final Event buttonEvent;
  private final GuiText displayText;
  private final Rectangle rectangle;
  private Image texture;
  private GuiElementMesh mesh;

  public Button(Observable observable, ButtonType buttonType, String texture, String text, int x, int y, int w, int h) {
    this(observable, buttonType, texture, text, x, y, w, h, TextureStretchStrategy.NONE);
  }

  public Button(Observable observable, ButtonType buttonType, String texture, String text, int x, int y, int w, int h, TextureStretchStrategy strategy) {
    observable.subscribe(this);
    this.buttonEvent = ButtonEvent.of(buttonType, this);
    if (texture != null) {
      this.texture = TextureUtil.loadImage("gui/button/" + texture, Channels.RGB);
    }
    this.rectangle = new Rectangle(x, y, w, h);
    this.displayText = new GuiText(text, rectangle, GuiText.PositioningStrategyType.XY_CENTER);
    this.mesh = GuiMeshGenerator.generateMesh(this.texture, strategy, w, h);

    if (strategy != TextureStretchStrategy.NONE && this.texture != null) {
      this.texture = removeIndicatorBorders(this.texture, strategy);
    }
  }

  private Image removeIndicatorBorders(Image texture, TextureStretchStrategy strategy) {
    int channels = texture.getChannels().toString().length();
    Image result = new Image(texture.getFileName());
    result.setChannels(texture.getChannels());
    result.setHeight(texture.getHeight() - 1);
    result.setWidth(texture.getWidth() - 1);

    if (strategy == TextureStretchStrategy.BOTH) {
      result.setImage(removeBothIndicatorBorders(texture.getImage(), texture.getHeight(), texture.getWidth(), channels));
      return result;
    }

    return null;
  }

  private ByteBuffer removeBothIndicatorBorders(ByteBuffer image, int height, int width, int channels) {
    ByteBuffer buffer = BufferUtils.createByteBuffer((height - 1) * (width - 1) * channels);
    byte[] row = new byte[width * channels];

    for (int i = 0; i < height - 1; i++) {
      image.get(row);
      buffer.put(row, channels, row.length - channels);
    }
    buffer.flip();

    return buffer;
  }

  @Override
  public void next(Event event) {
    if (event.getType() == EventType.MOUSE) {
      handleMouseEvent((MouseEvent) event);
    }
  }

  private void handleMouseEvent(MouseEvent mouseEvent) {
    if (mouseEvent instanceof MouseClickEvent) {
      handleMouseClickEvent((MouseClickEvent) mouseEvent);
    }
  }

  private void handleMouseClickEvent(MouseClickEvent event) {
    if (event.lmbReleased() && rectangle.inside(event.getX(), event.getY())) {
      LOG.debug("Button event: " + buttonEvent.getType().name());
      ArrayList<Observer> callList = new ArrayList<>(this.observers);
      callList.forEach(o -> o.next(buttonEvent));
    }
  }

  @Override
  public void subscribe(EventType eventType, Observer observer) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void subscribe(Observer observer) {
    observers.add(observer);
  }

  @Override
  public void unsubscribe(Observer observer) {
    observers.remove(observer);
  }

  @Override
  public Image getTexture() {
    return texture;
  }

  @Override
  public Rectangle getRectangle() {
    return rectangle;
  }

  @Override
  public GuiElementMesh getMesh() {
    return mesh;
  }

  public GuiText getElementText() {
    return displayText;
  }

  public void destroy() {
    observers.clear();
  }
}
