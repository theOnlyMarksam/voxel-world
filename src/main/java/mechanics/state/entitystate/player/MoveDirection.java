package mechanics.state.entitystate.player;

public enum MoveDirection {
  FORWARD,
  BACKWARD,
  UP,
  DOWN,
  LEFT,
  RIGHT
}
