package mechanics.state.entitystate.player;

import entity.Player;
import mechanics.event.Event;
import mechanics.state.entitystate.EntityState;

public class MoveState implements EntityState {
  private static final float SPEED = 10f;

  private final MoveDirection moveDirection;
  private Player player;

  public MoveState(MoveDirection moveDirection) {
    this.moveDirection = moveDirection;
  }

  public void setPlayer(Player player) {
    this.player = player;
  }

  @Override
  public void handleInput(Event event) {
    // empty for now
  }

  @Override
  public void update(float dt) {
    switch (moveDirection) {
      case FORWARD:
        player.moveForward(dt * SPEED);
        break;
      case BACKWARD:
        player.moveForward(-dt * SPEED);
        break;
      case LEFT:
        player.moveLeft(-dt * SPEED);
        break;
      case RIGHT:
        player.moveLeft(dt * SPEED);
        break;
      case UP:
        player.moveUp(dt * 2 * SPEED);
        break;
      case DOWN:
        player.moveUp(-dt * 2 * SPEED);
        break;
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    MoveState moveState = (MoveState) o;

    return moveDirection == moveState.moveDirection;

  }

  @Override
  public int hashCode() {
    return moveDirection.hashCode();
  }
}
