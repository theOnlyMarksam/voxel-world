package mechanics.state.entitystate;

import mechanics.event.Event;
import mechanics.state.State;

public interface EntityState extends State {

  // TODO move to State interface
  void handleInput(Event event);
}
