package mechanics.state;

public interface State {

  void update(float dt);
}
