package mechanics.state.gamestate;

import mechanics.observer.Observable;
import mechanics.observer.Observer;
import mechanics.state.State;

public interface GameState extends State, Observer, Observable {
  void init();

  void render();

  void pause();

  void resume();

  void destroy();
}
