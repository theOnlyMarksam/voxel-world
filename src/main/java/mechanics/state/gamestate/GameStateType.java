package mechanics.state.gamestate;



public enum GameStateType {
  MAIN(0),
  OPTION(1),
  PLAY(2),
  PAUSE(3),
  EXIT(4);

  final int index;

  GameStateType(int index) {
    this.index = index;
  }
}
