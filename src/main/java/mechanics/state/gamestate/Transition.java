package mechanics.state.gamestate;

public interface Transition {
  void apply();
}
