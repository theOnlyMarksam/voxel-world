package mechanics.state.gamestate.impl;

import main.Game;
import mechanics.element.Button;
import mechanics.element.TextureStretchStrategy;
import mechanics.event.button.ButtonType;
import mechanics.event.Event;
import mechanics.event.EventType;
import mechanics.observer.Observable;
import mechanics.observer.Observer;
import mechanics.state.gamestate.GameState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import renderengine.FontRenderer;
import renderengine.GuiRenderer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PauseMenuState implements GameState {
  private static final Logger LOG = LogManager.getFormatterLogger(PauseMenuState.class);

  private final List<Button> buttons;
  private Observable observable;
  private GuiRenderer renderer;
  private FontRenderer fontRenderer;
  private List<Observer> observers = new ArrayList<>();

  public PauseMenuState(Observable observable) {
    this.observable = observable;
    this.buttons = createButtons();
    this.renderer = new GuiRenderer();
    this.fontRenderer = new FontRenderer();
  }

  private List<Button> createButtons() {
    int width = 250;
    int height = 40;
    int center = Game.WIDTH / 2 - width / 2;
    int space = 30;
    int firstY = (Game.HEIGHT - 2 * space - 2 * height) / 2;

    Button resumeButton = new Button(
        this,
        ButtonType.RESUME,
        "stretch_btn.png",
        "Resume",
        center,
        firstY,
        width,
        height,
        TextureStretchStrategy.BOTH
    );

    Button mainMenuButton = new Button(
        this,
        ButtonType.EXIT,
        "stretch_btn.png",
        "Main menu",
        center,
        firstY + (height + space),
        width,
        height,
        TextureStretchStrategy.BOTH
    );

    return Arrays.asList(resumeButton, mainMenuButton);
  }

  @Override
  public void init() {
    renderer.setClearScreen(true);
    fontRenderer.setTextColor(0.0f, 0.0f, 0.0f);
    observable.subscribe(this);

    buttons.forEach(btn -> {
      renderer.bindGuiElement(btn);
      fontRenderer.bindText(btn.getElementText());
      btn.subscribe(this);
    });
  }

  @Override
  public void update(float dt) {

  }

  @Override
  public void render() {
    renderer.render();
    fontRenderer.render();
  }

  @Override
  public void pause() {
    observable.unsubscribe(this);
  }

  @Override
  public void resume() {
    observable.subscribe(this);
  }

  @Override
  public void destroy() {
    observable.unsubscribe(this);
    buttons.forEach(Button::destroy);
    renderer.destroy();
    fontRenderer.destory();
  }

  @Override
  public void next(Event event) {
    List<Observer> callList = observers.stream()
                                  .filter(observer -> !observer.equals(event.getEmitter()))
                                  .collect(Collectors.toList());
    callList.forEach(o -> o.next(event));
  }

  @Override
  public void subscribe(EventType eventType, Observer observer) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void subscribe(Observer observer) {
    observers.add(observer);
  }

  @Override
  public void unsubscribe(Observer observer) {
    observers.remove(observer);
  }
}
