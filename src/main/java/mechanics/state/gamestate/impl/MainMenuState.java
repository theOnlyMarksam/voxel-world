package mechanics.state.gamestate.impl;

import main.Game;
import mechanics.element.Button;
import mechanics.element.TextureStretchStrategy;
import mechanics.event.button.ButtonType;
import mechanics.event.Event;
import mechanics.event.EventType;
import mechanics.observer.Observable;
import mechanics.observer.Observer;
import mechanics.state.gamestate.GameState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import renderengine.FontRenderer;
import renderengine.GuiRenderer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MainMenuState implements GameState {
  private static final Logger LOG = LogManager.getFormatterLogger(MainMenuState.class);

  private final List<Button> buttons;
  private Observable observable;
  private GuiRenderer renderer;
  private FontRenderer fontRenderer;
  private List<Observer> observers = new ArrayList<>();

  public MainMenuState(Observable observable) {
    this.observable = observable;
    int width = 250;
    int height = 40;
    int center = Game.WIDTH / 2 - width / 2;
    int space = 30;
    int firstY = (Game.HEIGHT - 2 * space - 2 * height) / 2;

    Button playButton = new Button(
        this,
        ButtonType.START,
        "stretch_btn.png",
        "Play",
        center,
        firstY,
        width,
        height,
        TextureStretchStrategy.BOTH
    );

    Button exitButton = new Button(
        this,
        ButtonType.EXIT,
        "stretch_btn.png",
        "Exit",
        center,
        firstY + (height + space),
        width,
        height,
        TextureStretchStrategy.BOTH
    );

    this.buttons = Arrays.asList(playButton, exitButton);

    this.renderer = new GuiRenderer();
    this.renderer.setClearScreen(true);
    this.fontRenderer = new FontRenderer();
  }

  @Override
  public void init() {
    observable.subscribe(EventType.MOUSE, this);

    buttons.forEach(b -> {
      renderer.bindGuiElement(b);
      fontRenderer.bindText(b.getElementText());
      b.subscribe(this);
    });
  }

  @Override
  public void update(float dt) {

  }

  @Override
  public void render() {
    renderer.render();
    fontRenderer.render();
  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    observable.unsubscribe(this);
    buttons.forEach(Button::destroy);
    renderer.destroy();
    fontRenderer.destory();
  }

  @Override
  public void next(Event event) {
    List<Observer> callList = observers.stream()
                                 .filter(observer -> !observer.equals(event.getEmitter()))
                                 .collect(Collectors.toList());
    callList.forEach(o -> o.next(event));
  }

  @Override
  public void subscribe(EventType eventType, Observer observer) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void subscribe(Observer observer) {
    observers.add(observer);
  }

  @Override
  public void unsubscribe(Observer observer) {
    observers.remove(observer);
  }
}
