package mechanics.state.gamestate.impl;

import entity.CelestialBody;
import entity.Player;
import files.textures.Channels;
import files.textures.Image;
import files.textures.TextureMap;
import files.textures.TextureUtil;
import main.Game;
import mechanics.element.GuiElement;
import mechanics.element.GuiElementMesh;
import mechanics.element.GuiMeshGenerator;
import mechanics.element.Rectangle;
import mechanics.element.TextureStretchStrategy;
import mechanics.event.Event;
import mechanics.event.EventType;
import mechanics.event.key.KeyEvent;
import mechanics.event.mouse.MouseClickEvent;
import mechanics.event.mouse.MouseMoveEvent;
import mechanics.observer.Observable;
import mechanics.observer.Observer;
import mechanics.state.entitystate.player.MoveDirection;
import mechanics.state.entitystate.player.MoveState;
import mechanics.state.gamestate.GameState;
import model.ObservableWorld;
import model.block.BlockType;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;
import renderengine.GuiRenderer;
import renderengine.Renderer;

import java.util.ArrayList;
import java.util.List;

public class PlayState implements GameState {
  private static final float WAVE_SPEED = 0.03f;

  private float waveLocation = 0f;

  private ObservableWorld world;
  private Renderer renderer;
  private Player player;
  private TextureMap textureMap = new TextureMap();

  private Vector2f lastMousePosition = null;
  private Vector2f rotation = new Vector2f(0.0f, 0.0f);
  private CelestialBody sun;
  private CelestialBody moon;
  private Observable observable;
  private GuiRenderer guiRenderer;
  private List<Observer> observerList = new ArrayList<>();
  private GuiElement pointer;

  public PlayState(Observable observable) {
    this.observable = observable;

    this.player = new Player(new Vector3f(0.0f, 35.0f, 0.0f));

    this.sun = new CelestialBody("sun.png", player);
    this.moon = new CelestialBody("moon.png", player);

    this.world = new ObservableWorld(player, textureMap);
    this.renderer = new Renderer(player.getCamera(), sun, moon, textureMap);
    this.guiRenderer = new GuiRenderer();
    this.pointer = new Pointer();
  }

  @Override
  public void init() {
    observable.subscribe(EventType.KEY, this);
    observable.subscribe(EventType.MOUSE, this);

    textureMap.initTextureMap();
    world.init();
    sun.addRotation((float) Math.PI);

    guiRenderer.bindGuiElement(pointer);
    guiRenderer.setBlending(true);
  }

  @Override
  public void update(float dt) {
    rotation.mul(dt * 0.2f);

    player.rotateView(rotation.x, rotation.y);
    player.update(dt);

    rotation.set(0.0f);

    player.getCamera().update();
    world.update();

    sun.update(dt);
    moon.update(dt);

    updateWave(dt);

    renderer.addVertexArrays(world.getNewOrChangedMeshes());
    renderer.deleteVertexArrays(world.getDeletedMeshCoordinates());
    renderer.setWaveLocation(waveLocation);
  }

  private void updateWave(float dt) {
    waveLocation += WAVE_SPEED * dt;
    waveLocation %= 1;
  }

  @Override
  public void render() {
    renderer.render();
    guiRenderer.render();
  }

  @Override
  public void pause() {
    observable.unsubscribe(this);
    renderer.addVertexArrays(world.getNewOrChangedMeshes());
    renderer.deleteVertexArrays(world.getDeletedMeshCoordinates());
    guiRenderer.destroy();
    player.clearStates();
    lastMousePosition = null;
  }

  @Override
  public void resume() {
    observable.subscribe(EventType.KEY, this);
    observable.subscribe(EventType.MOUSE, this);
    guiRenderer.bindGuiElement(pointer);
  }

  @Override
  public void destroy() {
    observable.unsubscribe(this);
    world.shutdown();
    renderer.shutDown();
    guiRenderer.destroy();
    player.clearStates();
    lastMousePosition = null;
  }

  @Override
  public void next(Event event) {
    if (event.getType() == EventType.MOUSE) {
      handleMouseEvent(event);
    } else if (event.getType() == EventType.KEY) {
      handleKeyboardEvent((KeyEvent) event);
    }
  }

  private void handleMouseEvent(Event event) {
    if (event instanceof MouseClickEvent) {
      handleMouseClick((MouseClickEvent) event);
    } else if (event instanceof MouseMoveEvent) {
      handleMouseMoveEvent((MouseMoveEvent) event);
    }
  }

  private void handleMouseClick(MouseClickEvent event) {
    if (event.lmbPressed()) {
      world.setDestroyBlocks(true);
    } else if (event.lmbReleased()) {
      world.setDestroyBlocks(false);
    } else if (event.rmbPressed()) {
      world.setAddBlocks(true);
    } else if (event.rmbReleased()) {
      world.setAddBlocks(false);
    }
  }

  private void handleMouseMoveEvent(MouseMoveEvent event) {
    if (lastMousePosition != null) {
      rotation.set(lastMousePosition.y - event.getY(), lastMousePosition.x - event.getX());
      lastMousePosition.set(event.getX(), event.getY());
    } else {
      lastMousePosition = new Vector2f(event.getX(), event.getY());
    }
  }

  private void handleKeyboardEvent(KeyEvent event) {
    if (event.getAction() == GLFW.GLFW_PRESS) {
      handleKeyPressEvent(event);
    } else if (event.getAction() == GLFW.GLFW_RELEASE) {
      handleKeyReleaseEvent(event);
    }
  }

  private void handleKeyPressEvent(KeyEvent event) {
    if (event.getButton() == GLFW.GLFW_KEY_ESCAPE) {
      List<Observer> callList = new ArrayList<>(observerList);
      callList.forEach(observer -> observer.next(event));
    } else if (event.getButton() == GLFW.GLFW_KEY_W) {
      player.addState(new MoveState(MoveDirection.FORWARD));
    } else if (event.getButton() == GLFW.GLFW_KEY_S) {
      player.addState(new MoveState(MoveDirection.BACKWARD));
    } else if (event.getButton() == GLFW.GLFW_KEY_A) {
      player.addState(new MoveState(MoveDirection.LEFT));
    } else if (event.getButton() == GLFW.GLFW_KEY_D) {
      player.addState(new MoveState(MoveDirection.RIGHT));
    } else if (event.getButton() == GLFW.GLFW_KEY_LEFT_SHIFT) {
      player.addState(new MoveState(MoveDirection.DOWN));
    } else if (event.getButton() == GLFW.GLFW_KEY_SPACE) {
      player.addState(new MoveState(MoveDirection.UP));
    } else if (event.getButton() == GLFW.GLFW_KEY_1) {
      world.setBlockToAdd(BlockType.DIRT);
    } else if (event.getButton() == GLFW.GLFW_KEY_2) {
      world.setBlockToAdd(BlockType.GRASS);
    } else if (event.getButton() == GLFW.GLFW_KEY_3) {
      world.setBlockToAdd(BlockType.SAND);
    } else if (event.getButton() == GLFW.GLFW_KEY_4) {
      world.setBlockToAdd(BlockType.STONE);
    } else if (event.getButton() == GLFW.GLFW_KEY_5) {
      world.setBlockToAdd(BlockType.WATER);
    }
  }

  private void handleKeyReleaseEvent(KeyEvent event) {
    if (event.getButton() == GLFW.GLFW_KEY_W) {
      player.removeState(new MoveState(MoveDirection.FORWARD));
    } else if (event.getButton() == GLFW.GLFW_KEY_S) {
      player.removeState(new MoveState(MoveDirection.BACKWARD));
    } else if (event.getButton() == GLFW.GLFW_KEY_A) {
      player.removeState(new MoveState(MoveDirection.LEFT));
    } else if (event.getButton() == GLFW.GLFW_KEY_D) {
      player.removeState(new MoveState(MoveDirection.RIGHT));
    } else if (event.getButton() == GLFW.GLFW_KEY_LEFT_SHIFT) {
      player.removeState(new MoveState(MoveDirection.DOWN));
    } else if (event.getButton() == GLFW.GLFW_KEY_SPACE) {
      player.removeState(new MoveState(MoveDirection.UP));
    }
  }

  @Override
  public void subscribe(EventType eventType, Observer observer) {

  }

  @Override
  public void subscribe(Observer observer) {
    observerList.add(observer);
  }

  @Override
  public void unsubscribe(Observer observer) {
    observerList.remove(observer);
  }

  private class Pointer implements GuiElement {
    private Image image;
    private GuiElementMesh mesh;
    private Rectangle rectangle;

    Pointer() {
      this.image = TextureUtil.loadImage("gui/pointer.png", Channels.RGBA);

      int w = image.getWidth();
      int h = image.getHeight();
      this.mesh = GuiMeshGenerator.generateMesh(image, TextureStretchStrategy.NONE, w, h);
      this.rectangle = new Rectangle((Game.WIDTH - w) / 2, (Game.HEIGHT - h) / 2, w, h);
    }

    @Override
    public Image getTexture() {
      return image;
    }

    @Override
    public Rectangle getRectangle() {
      return rectangle;
    }

    @Override
    public GuiElementMesh getMesh() {
      return mesh;
    }
  }
}
