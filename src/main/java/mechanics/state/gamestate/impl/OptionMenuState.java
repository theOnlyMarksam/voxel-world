package mechanics.state.gamestate.impl;

import mechanics.event.Event;
import mechanics.event.EventType;
import mechanics.observer.Observable;
import mechanics.observer.Observer;
import mechanics.state.gamestate.GameContext;
import mechanics.state.gamestate.GameState;

public class OptionMenuState implements GameState {
  public OptionMenuState(Observable observable) {

  }

  @Override
  public void init() {

  }

  @Override
  public void update(float dt) {

  }

  @Override
  public void render() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void destroy() {

  }

  @Override
  public void next(Event event) {

  }

  @Override
  public void subscribe(EventType eventType, Observer observer) {

  }

  @Override
  public void subscribe(Observer observer) {

  }

  @Override
  public void unsubscribe(Observer observer) {

  }
}
