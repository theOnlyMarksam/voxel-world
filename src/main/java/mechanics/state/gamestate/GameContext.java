package mechanics.state.gamestate;

import mechanics.event.button.ButtonEvent;
import mechanics.event.button.ButtonType;
import mechanics.event.Event;
import mechanics.event.EventType;
import mechanics.event.key.KeyEvent;
import mechanics.event.mouse.MouseClickEvent;
import mechanics.event.mouse.MouseEnterEvent;
import mechanics.event.mouse.MouseExitEvent;
import mechanics.event.mouse.MouseMoveEvent;
import mechanics.observer.Observable;
import mechanics.observer.Observer;
import mechanics.state.gamestate.impl.MainMenuState;
import mechanics.state.gamestate.impl.OptionMenuState;
import mechanics.state.gamestate.impl.PauseMenuState;
import mechanics.state.gamestate.impl.PlayState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.lwjgl.glfw.GLFW.GLFW_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_DISABLED;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_NORMAL;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwSetCursorEnterCallback;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetInputMode;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;

public class GameContext implements Observable, Observer {
  private static final Logger LOG = LogManager.getFormatterLogger(GameContext.class);
  private final Transition emptyTransition = () -> {
  };

  private long window;
  private GameState[] gameStates;
  private Stack<GameStateType> current = new Stack<>();

  private Map<GameStateType, Map<Event, Transition>> gameStateTransitions;
  private Queue<Event> events = new LinkedList<>();
  private Map<EventType, List<Observer>> observers;

  public void init(long window) {
    this.window = window;
    gameStates = new GameState[GameStateType.values().length];
    observers = initObserverMap();

    gameStates[GameStateType.MAIN.index] = new MainMenuState(this);
    gameStates[GameStateType.OPTION.index] = new OptionMenuState(this);
    gameStates[GameStateType.PLAY.index] = new PlayState(this);
    gameStates[GameStateType.PAUSE.index] = new PauseMenuState(this);

    Arrays.stream(gameStates).forEach(state -> {
      if (state != null) {
        state.subscribe(this);
      }
    });

    current.push(GameStateType.MAIN);
    gameStates[current.peek().index].init();

    gameStateTransitions = initGameStateTransitions();

    glfwSetKeyCallback(window, this::keyboardInput);
    glfwSetMouseButtonCallback(window, this::mouseInput);
    glfwSetCursorPosCallback(window, this::mouseMove);
    glfwSetCursorEnterCallback(window, this::cursorEnter);
  }

  private Map<GameStateType, Map<Event, Transition>> initGameStateTransitions() {
    return Map.of(
        GameStateType.MAIN, getMainMenuTransitions(),
        GameStateType.OPTION, getOptionMenuTransitions(),
        GameStateType.PAUSE, getPauseMenuTransitions(),
        GameStateType.PLAY, getPlayStateTransitions()
    );

  }

  private Map<EventType, List<Observer>> initObserverMap() {
    Map<EventType, List<Observer>> observers = new HashMap<>();
    EnumSet.allOf(EventType.class).forEach(eventType -> observers.put(eventType, new ArrayList<>()));

    return observers;
  }

  private Map<Event, Transition> getMainMenuTransitions() {
    return Map.of(
        ButtonEvent.of(ButtonType.EXIT, null), () -> {
          gameStates[current.pop().index].destroy();
          glfwSetWindowShouldClose(window, true);
        },
        ButtonEvent.of(ButtonType.START, null), () -> {
          gameStates[current.pop().index].destroy();
          gameStates[current.push(GameStateType.PLAY).index].init();
          hideCursor();
        },
        ButtonEvent.of(ButtonType.OPTIONS, null), () -> {
          gameStates[current.peek().index].pause();
          gameStates[current.push(GameStateType.OPTION).index].init();
        }
    );
  }

  private Map<Event, Transition> getOptionMenuTransitions() {
    return Map.of(
        ButtonEvent.of(ButtonType.EXIT, null), () -> {
          gameStates[current.pop().index].destroy();
          gameStates[current.peek().index].resume();
        },
        KeyEvent.of(GLFW_KEY_ESCAPE, GLFW_PRESS, null), () -> {
          gameStates[current.pop().index].destroy();
          gameStates[current.peek().index].resume();
        }
    );
  }

  private Map<Event, Transition> getPauseMenuTransitions() {
    return Map.of(
        ButtonEvent.of(ButtonType.EXIT, null), () -> {
          gameStates[current.pop().index].destroy();
          gameStates[current.pop().index].destroy();
          gameStates[current.push(GameStateType.MAIN).index].init();
        },
        ButtonEvent.of(ButtonType.OPTIONS, null), () -> {
          gameStates[current.peek().index].pause();
          gameStates[current.push(GameStateType.OPTION).index].init();
        },
        ButtonEvent.of(ButtonType.RESUME, null), () -> {
          gameStates[current.pop().index].destroy();
          gameStates[current.peek().index].resume();
          hideCursor();
        },
        KeyEvent.of(GLFW_KEY_ESCAPE, GLFW_PRESS, null), () -> {
          gameStates[current.pop().index].destroy();
          gameStates[current.peek().index].resume();
          hideCursor();
        }
    );
  }

  private Map<Event, Transition> getPlayStateTransitions() {
    return Map.of(
        KeyEvent.of(GLFW_KEY_ESCAPE, GLFW_PRESS, null), () -> {
          gameStates[current.peek().index].pause();
          gameStates[current.push(GameStateType.PAUSE).index].init();
          showCursor();
        }
    );
  }

  private void hideCursor() {
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  }

  private void showCursor() {
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
  }

  public void update(float delta) {
    while (events.peek() != null) {
      Event e = events.poll();
      Stream.concat(observers.get(EventType.DEFAULT).stream(), observers.get(e.getType()).stream())
          .filter(observer -> !observer.equals(e.getEmitter()))
          .collect(Collectors.toList())
          .forEach(o -> o.next(e));

    }
    if (!current.empty()) gameStates[current.peek().index].update(delta);
  }

  public void render() {
    if (!current.empty()) gameStates[current.peek().index].render();
  }

  @Override
  public void subscribe(EventType eventType, Observer observer) {
    observers.get(eventType).add(observer);
  }

  @Override
  public void subscribe(Observer observer) {
    observers.get(EventType.DEFAULT).add(observer);
  }

  @Override
  public void unsubscribe(Observer observer) {
    for (List<Observer> observerList : observers.values()) {
      observerList.remove(observer);
    }
  }

  private void keyboardInput(long window, int key, int scanCode, int action, int mods) {
    if (action == GLFW_PRESS || action == GLFW_RELEASE) {
      LOG.debug("Keyboard event: {key: %d, action: %d}", key, action);
    }
    KeyEvent e = KeyEvent.of(key, action, this);
    if (!consume(e)) {
      events.add(e);
    }
  }

  private boolean consume(KeyEvent e) {
    Map<Event, Transition> eventTransitionMap = gameStateTransitions.get(current.peek());
    if (eventTransitionMap.containsKey(e)) {
      next(e);
      return true;
    }
    return false;
  }

  private void mouseInput(long window, int button, int action, int mods) {
    double[] x = new double[1];
    double[] y = new double[1];
    glfwGetCursorPos(window, x, y);
    Event e = MouseClickEvent.of(button, action, (int) x[0], (int) y[0], this);
    LOG.debug("Mouse clicked: (%.2f, %.2f)", x[0], y[0]);
    events.add(e);
  }

  private void mouseMove(long window, double xPos, double yPos) {
    Event e = MouseMoveEvent.of(xPos, yPos, this);
    events.add(e);
  }

  private void cursorEnter(long window, boolean entered) {
    LOG.debug("Cursor entered: %b", entered);

    Event e;
    if (entered) {
      e = MouseEnterEvent.of(this);
    } else {
      e = MouseExitEvent.of(this);
    }

    events.add(e);
  }

  @Override
  public void next(Event event) {
    LOG.debug(event.toString());

    Map<Event, Transition> eventTransitionMap = gameStateTransitions.get(current.peek());

    if (event instanceof ButtonEvent) {
      ButtonEvent e = (ButtonEvent) event;
      eventTransitionMap.getOrDefault(e, emptyTransition).apply();
    } else if (event instanceof KeyEvent) {
      KeyEvent e = (KeyEvent) event;
      eventTransitionMap.getOrDefault(e, emptyTransition).apply();
    }
  }
}
