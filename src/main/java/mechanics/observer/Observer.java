package mechanics.observer;

import mechanics.event.Event;

public interface Observer {
  void next(Event event);
}
