package mechanics.observer;

import mechanics.event.EventType;

public interface Observable {

  void subscribe(EventType eventType, Observer observer);

  void subscribe(Observer observer);

  void unsubscribe(Observer observer);
}
