package common.conversion;

import model.chunk.Chunk;
import model.coordinate.ChunkCoordinate;
import model.coordinate.ChunkGlobalCoordinate;
import model.coordinate.ChunkLocalCoordinate;
import model.coordinate.GlobalCoordinate;
import org.joml.Vector3f;

public class CoordinateConverter {

  public static GlobalCoordinate chunkGlobalToGlobal(ChunkGlobalCoordinate chunkGlobalCoordinate) {
    long x = chunkGlobalCoordinate.getLocalX() + chunkGlobalCoordinate.getChunkX() * Chunk.SIDE_LENGTH;
    long y = chunkGlobalCoordinate.getLocalY();
    long z = chunkGlobalCoordinate.getLocalZ() + chunkGlobalCoordinate.getChunkZ() * Chunk.SIDE_LENGTH;

    return GlobalCoordinate.of(x, y, z);
  }

  public static GlobalCoordinate chunkLocalToGlobal(ChunkLocalCoordinate chunkLocalCoordinate,
                                                    ChunkCoordinate chunkCoordinate) {
    long x = chunkLocalCoordinate.x() + chunkCoordinate.x() * Chunk.SIDE_LENGTH;
    long y = chunkLocalCoordinate.y();
    long z = chunkLocalCoordinate.z() + chunkCoordinate.z() * Chunk.SIDE_LENGTH;

    return GlobalCoordinate.of(x, y, z);
  }

  public static GlobalCoordinate chunkLocalToGlobal(int blockIndex,
                                                    ChunkCoordinate chunkCoordinate) {
    ChunkLocalCoordinate chunkLocalCoordinate = ChunkLocalCoordinate.of(blockIndex);
    return chunkLocalToGlobal(chunkLocalCoordinate, chunkCoordinate);
  }

  public static ChunkGlobalCoordinate globalToChunkGlobal(GlobalCoordinate globalCoordinate) {
    int chunkX = (int) Math.floorDiv(globalCoordinate.x(), Chunk.SIDE_LENGTH);
    int chunkZ = (int) Math.floorDiv(globalCoordinate.z(), Chunk.SIDE_LENGTH);

    int localX = (int) (globalCoordinate.x() - chunkX * Chunk.SIDE_LENGTH);
    int localY = (int) globalCoordinate.y();
    int localZ = (int) (globalCoordinate.z() - chunkZ * Chunk.SIDE_LENGTH);

    return ChunkGlobalCoordinate.of(chunkX, chunkZ, localX, localY, localZ);
  }

  public static ChunkCoordinate globalToChunk(Vector3f globalCoordinate) {
    int chunkX = Math.floorDiv((int) globalCoordinate.x, Chunk.SIDE_LENGTH);
    int chunkZ = Math.floorDiv((int) globalCoordinate.z, Chunk.SIDE_LENGTH);

    return ChunkCoordinate.of(chunkX, chunkZ);
  }

  public static ChunkGlobalCoordinate vectorToChunkGlobal(Vector3f globalCoordinate) {
    GlobalCoordinate c = GlobalCoordinate.of((int) globalCoordinate.x, (int) globalCoordinate.y, (int) globalCoordinate.z);
    return globalToChunkGlobal(c);
  }
}
