#version 450 core

in vec4 clipSpace;
in vec2 interpolatedUv;
in vec3 toCameraVector;
in vec3 fromLightVector;

out vec4 finalColor;

uniform sampler2D reflectionTexture;
uniform sampler2D refractionTexture;
uniform sampler2D dudvMap;
uniform sampler2D normalMap;
uniform sampler2D depthMap;
uniform float moveFactor;
uniform vec3 lightColor;

const float waveStrength = 0.02;
const float shineDamper = 20.0;
const float reflectivity = 0.6;
const float near = 0.3; // TODO load as uniform
const float far = 350.0; // TODO load as uniform

float convertToDistance(float depth) {
    return 2.0 * near * far / (far + near - (2.0 * depth - 1.0) * (far - near));
}

void main(void) {
    vec2 ndc = (clipSpace.xy / clipSpace.w) / 2 + 0.5;
    vec2 reflectTexCoords = vec2(ndc.x, 1-ndc.y);
    vec2 refractTexCoords = vec2(ndc.x, ndc.y);

    float depth = texture(depthMap, refractTexCoords).r;
    float floorDistance = convertToDistance(depth);

    depth = gl_FragCoord.z;
    float waterDistance = convertToDistance(depth);
    float waterDepth = floorDistance - waterDistance;

    vec2 distortedTexCoords = texture(dudvMap, vec2(interpolatedUv.x + moveFactor, interpolatedUv.y)).rg * 0.1;
    distortedTexCoords = interpolatedUv + vec2(distortedTexCoords.x, distortedTexCoords.y + moveFactor);
    vec2 totalDistortion = (texture(dudvMap, distortedTexCoords).rg * 2.0 - 1.0) * waveStrength * clamp(waterDepth / 5.0, 0.6, 1.0);

    reflectTexCoords += totalDistortion;
    refractTexCoords += totalDistortion;

    vec4 reflectColor = texture(reflectionTexture, reflectTexCoords);
    vec4 refractColor = texture(refractionTexture, refractTexCoords);

    vec4 normalMapColor = texture(normalMap, distortedTexCoords);
    vec3 normal = vec3(normalMapColor.r * 2.0 - 1.0, normalMapColor.b * 3.0, normalMapColor.g * 2.0 - 1.0);
    normal = normalize(normal);

    vec3 viewVector = normalize(toCameraVector);
    float transparencyFactor = dot(viewVector, normal);

    vec3 reflectedLight = reflect(normalize(fromLightVector), normal);
    float specular = max(dot(reflectedLight, viewVector), 0.0);
    specular = pow(specular, shineDamper);
    vec3 specularHighlights = lightColor * specular * reflectivity * clamp(waterDepth / 5.0, 0.6, 1.0);

    finalColor = mix(reflectColor, refractColor, transparencyFactor);
    finalColor = mix(finalColor, vec4(0.0, 0.3, 0.5, 1.0), 0.2) + vec4(specularHighlights, 0.0);
    finalColor.a = clamp(waterDepth / 5.0, 0.6, 1.0);
}
