#version 450 core

uniform sampler2DArray tex;
uniform sampler2D shadowTexture;
uniform vec3 lightPos;

in vec3 interpolatedUv;
in vec3 interpolatedNormal;
in vec3 fragPos;
in vec4 fragPosLightSpace;

out vec4 finalColor;

float shadow(vec4 currentPos, int pcfCount) {
    vec4 pos = currentPos * 0.5 + 0.5;

    // keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
    if(pos.z > 1.0)
        return 0.0;

    float totalTexels = (pcfCount * 2.0 + 1.0) * (pcfCount * 2.0 + 1.0);
    vec2 texelSize = 1.0 / textureSize(shadowTexture, 0);
    float total = 0.0;

    for(int x = -pcfCount; x <= pcfCount; x++) {
        for(int y = -pcfCount; y <= pcfCount; y++) {
            float objectNearestLight = texture(shadowTexture, pos.xy + vec2(x, y) * texelSize).r;
            total += pos.z - 0.005 > objectNearestLight  ? 1.0 : 0.0;
        }
    }

    return total / totalTexels * 0.00000001;
}

void main() {
    vec3 up = vec3(0.0, 1.0, 0.0);

    vec3 n = normalize(interpolatedNormal);
    vec3 l = normalize(lightPos - fragPos);
    vec4 color = texture(tex, interpolatedUv);
    vec3 lightColor = vec3(0.7);

    vec3 ambient = vec3(0.3);

    float diff = max(dot(n, l), 0.0);
    vec3 diffuse = diff * lightColor;

    float lightFactor = shadow(fragPosLightSpace, 1);
    vec3 lighting = (ambient + (1 - lightFactor) * diffuse) * color.rgb;

    finalColor = vec4(lighting, color.a);
}
