#version 450 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 uv;

out vec3 interpolatedUv;
out vec3 interpolatedNormal;
out vec3 fragPos;
out vec4 fragPosLightSpace;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 lightSpaceMatrix;
uniform vec4 clipPlane;

void main() {
    vec4 worldPosition = vec4(position, 1.0);

    gl_ClipDistance[0] = dot(worldPosition, clipPlane);

    interpolatedUv = uv;
    interpolatedNormal = normal;
    fragPos = position;

    fragPosLightSpace = lightSpaceMatrix * vec4(position, 1.0);
    gl_Position = projection * view * worldPosition;
}
