#version 450 core

in vec2 interpolatedUv;

layout(binding=0) uniform sampler2D tex;

void main() {
    gl_FragColor = vec4(texture(tex, interpolatedUv).rgb, 1.0);
}