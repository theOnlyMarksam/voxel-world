#version 450 core

layout(binding=0) uniform sampler2D guiTexture;
in vec2 interpolatedUv;

out vec4 finalColor;

void main(void) {
    finalColor = texture(guiTexture, interpolatedUv);
}
