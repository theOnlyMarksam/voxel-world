#version 450 core

in vec3 position;
in vec2 uv;

out vec2 interpolatedUv;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main() {
    gl_Position = projection * view * model * vec4(position, 1.0);
    interpolatedUv = uv;
}