#version 450 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 uv;

out vec4 clipSpace;
out vec2 interpolatedUv;
out vec3 toCameraVector;
out vec3 fromLightVector;

uniform mat4 projection;
uniform mat4 view;
uniform vec3 cameraPosition;
uniform vec3 lightPosition;

const float tiling = 1.0 / 32.0;

void main(void) {
    vec4 worldPos = vec4(position, 1.0);

    clipSpace = projection * view * worldPos;
    interpolatedUv = position.xz * tiling;
    gl_Position = clipSpace;

    toCameraVector = cameraPosition - worldPos.xyz;
    fromLightVector = worldPos.xyz - lightPosition;
}
