#version 450 core

in vec2 interpolatedUv;

out vec4 result;

uniform sampler2D fontTexture;
uniform vec3 textColor;

void main() {
    vec4 color = texture(fontTexture, interpolatedUv);
    result = vec4(textColor.r * color.r, textColor.g * color.r, textColor.b * color.r, color.r);
}
