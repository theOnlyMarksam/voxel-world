# Voxel-World #
Voxel-world is a Minecraft like game. Project uses [LWJGL](https://www.lwjgl.org/) to access OpenGL functions.

## Running ##
<code>gradlew run</code> to download dependencies and run the application

NB! <code>JDK 11</code> is required to build and run the project
